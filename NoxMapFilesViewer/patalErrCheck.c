
#include "patalErrCheck.h"

int PatalErrorCheck()
{
	//TODO: 언어 파일을 먼저 읽어들입니다
	if (ReadLanguageFile("MapsView\\mapsViewLang.bmp", MainProcPtr))
	{
		//TODO: 동일한 프로세스가 있는지 검사합니다
		if (MainProcPtr->existProcErr)
		{
			MyQuestionBox(NULL, 1, 3, MB_OK | MB_ICONERROR);
			ClearStringDataFromMemory(MainProcPtr);
			return 0;
		}
		else
			return 1;
	}
	return 0;
}