
#include "mainHead.h"

LPCWSTR lpszClass = L"NOX MapFile Viewer //TODO: Happy Soft LTD";
//LPCWSTR lpszClass = L"NOX Quest_maps_Manager --happy soft ltd";

LPCWSTR MutexSeqID = L"//TODO: NOX MapFile Viewer --HappySoftLTD";

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	HWND hWnd;
	MSG Message;
	WNDCLASS WndClass;
	HANDLE hMutex = CreateMutex(NULL, TRUE, MutexSeqID);
	MyMainProc mProc;
	MainFormWnd mFormWnd;

	g_hInst = hInstance;
	
	MainProcPtr = &mProc;
	mProc.mFormPtr = &mFormWnd;
	mProc.existProcErr = (GetLastError() == ERROR_ALREADY_EXISTS);
	if (!PatalErrorCheck())
		return 0;
	MapfileViewerInit(&mProc);
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hbrBackground = (HBRUSH)GetStockObject(0);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));
	WndClass.hInstance = hInstance;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.lpszClassName = lpszClass;
	WndClass.lpszMenuName = NULL;
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	RegisterClass(&WndClass);

	hWnd = CreateWindow(lpszClass, lpszClass, WS_CAPTION | WS_SYSMENU, //WS_OVERLAPPEDWINDOW WS_VSCROLL,
		CW_USEDEFAULT, 0, PickMyData(107), PickMyData(108), NULL, (HMENU)NULL, hInstance, NULL);
	ShowWindow(hWnd, nCmdShow);

	SetWindowText(hWnd, PickMyWideStringFromIndex(0));
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}
	ReleaseMutex(hMutex);
	CloseHandle(hMutex);
	return Message.wParam;
}