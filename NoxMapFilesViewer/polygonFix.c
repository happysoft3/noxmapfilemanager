

#include "polygonFix.h"
#include "readMapDetail.h"
#include "pairTile.h"
#include "MapCompress.h"

int CheckPointDuplicate(tPoint *pHead, char *pointObject)
{
	int index = *(int *)pointObject, res = 0;
	int xProfile = *(int *)(pointObject + 4);
	int yProfile = *(int *)(pointObject + 8);
	tPoint *tCur = pHead->pNext;

	while (tCur != NULL)
	{
		if (xProfile == tCur->x && yProfile == tCur->y)
		{
			res = tCur->edge;
			RemoveDupleNode(tCur);
			break;
		}
		tCur = tCur->pNext;
	}
	return res;
}

void AddPolygonPoint(tPoint *pHead, char *pointObject)
{
	int index = *(int *)pointObject, res = 0;
	int xProfile = *(int *)(pointObject + 4);
	int yProfile = *(int *)(pointObject + 8);
	tPoint *pNode = (tPoint *)malloc(sizeof(tPoint));

	pNode->x = xProfile;
	pNode->y = yProfile;
	pNode->edge = index;
	pNode->pNext = pHead->pNext;
	if (pHead->pNext != NULL)
		pHead->pNext->pPrev = pNode;
	pNode->pPrev = pHead;
	pHead->pNext = pNode;
	pHead->x++;
}

int ReallocPolygonPoint(char *stream, int pointCount, int **dupArr, char *output, int *bitwr)
{
	tPoint pHead;
	char *pointObject = stream;
	int pointRes = 0;
	
	pHead.x = 0;
	pHead.pNext = NULL;
	if (pointCount)
	{
		*dupArr = (int *)malloc(sizeof(int) * (pointCount + 1));
		memset(*dupArr, 0, sizeof(int) * (pointCount + 1));
	}
	for (int i = 0; i < pointCount; i++)
	{
		int dupNum = CheckPointDuplicate(&pHead, pointObject);
		if (dupNum)
		{
			*dupArr[i + 1] = dupNum;
		}
		else
		{
			AddPolygonPoint(&pHead, pointObject);
			BitWriteOnMemory(pointObject, output, bitwr, 0xc);
		}
		pointObject += 0xc;
	}
	pointRes = pHead.x;
	RemoveAllPointList(pHead.pNext);
	return pointRes;
}

int PolygonPointSubstitution(char *pointGroup, int *dupArr)
{
	unsigned short pointCount = *(short *)pointGroup;

	pointGroup += 2;
	for (int i = 0; i < (int)pointCount; i++)
	{
		int *curPoint = (int *)(pointGroup + (i * 4));
		if (dupArr[*curPoint])
			*curPoint = dupArr[*curPoint];
	}
	return pointCount;
}

int CheckIncorrectFlag(int flag)
{
	return (flag & 0xff) == 4 && ((flag >> 0x10) ^ 2);
}

int PolygonModifier(char *stream, int pointCount, char *output, int *bitwr, int *dupArr, int flags)
{
	char *pic = stream + (pointCount * 12);
	int *polygonCount = (int *)pic;

	BitWriteOnMemory(pic, output, bitwr, sizeof(int));
	pic += 4;
	for (int i = 0; i < *polygonCount; i++)
	{
		char *temp = pic;
		unsigned char nameLen = pic[0];
		pic += (nameLen + 1 + 4);
		int pointCount = PolygonPointSubstitution(pic, dupArr);
		int pointSectSize = pointCount * 4 + 2;
		pic += (pointSectSize + 2);
		int entryScr1 = *(int *)pic;
		pic += (4 + entryScr1 + 4 + 2);
		int entryScr2 = *(int *)pic;
		pic += (4 + entryScr2 + 4);
		if (CheckIncorrectFlag(flags))
		{
			BitWriteOnMemory(temp, output, bitwr, pic - temp);
			pic += 4;
			continue;
		}
		else if ((flags >> 0x10) == 2)
			pic += 4;
		BitWriteOnMemory(temp, output, bitwr, pic - temp);
	}
	return (int)pic;
}

int PolygonHandler(char *stream, char *output, int *bitwr, int questMode)
{
	int cur = 0;
	unsigned short modeFlag = (*(short *)stream) | (questMode << 0x10);
	int *pointCount = (int *)(stream + 2);
	int *dupArr = NULL;
	int destWrt = *bitwr;
	
	BitWriteOnMemory(stream, output, bitwr, 6);
	if (CheckIncorrectFlag(modeFlag))
		output[destWrt] = 3;
	*pointCount = ReallocPolygonPoint(stream + 6, *pointCount, &dupArr, output, bitwr);
	PolygonModifier(stream + 6, *pointCount, output, bitwr, dupArr, modeFlag);
	if (dupArr != NULL)
		free(dupArr);
	return destWrt;
}

int PolygonSectionCompress(char *stream, char *symb, int curpos, char *output, int *bitwr)
{
	char *ptr = stream + curpos;
	int symbLen = strlen(symb) + 1, pos;
	int *sectionSize = NULL, resSize;
	int tempWrt, tempWrt2;

	if (symbLen == (int)ptr[0])
	{
		if (CheckSymbolMatching(ptr + 1, symb, symbLen))
		{
			pos = Get8BytePosition(ptr + symbLen - stream);
			sectionSize = (int *)(stream + pos);
			BitWriteOnMemory(ptr, output, bitwr, pos - curpos + 8);
			tempWrt = *bitwr;
			tempWrt2 = PolygonHandler(stream + pos + 8, output, bitwr, *(int *)(stream + 0x5a2)); //Fix
			resSize = *bitwr - tempWrt;
			memcpy_s(output + tempWrt2 - 8, sizeof(int), &resSize, sizeof(int));
			pos += (8 + *sectionSize);
			return pos - curpos;
		}
	}
	return -1;
}