#pragma once

#include "mainHead.h"

typedef struct _tilePoint
{
	int x;
	int y;
	int edge;
	struct _tilePoint *pNext;
	struct _tilePoint *pPrev;
}tPoint;

void RemoveDupleNode(tPoint *pDelNode);
int CheckDuplicateCoor(tPoint *pHead, int xyProfile);
void AddTilePointList(tPoint *pHead, int xyProfile, int edgeCount);
void RemoveAllPointList(tPoint *tCur);