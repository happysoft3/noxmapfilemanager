
#include "mapViewProc.h"
#include "loadDir.h"
#include "myNodeKey.h"
#include "analysisDlg.h"


void ClearAllMapNodes(NoxMapNode *removedNode)
{
	if (removedNode != NULL)
	{
		ClearAllMapNodes(removedNode->nextMap);
		free(removedNode);
	}
}

void EmptyMapNodes(MyMainProc *mProc)
{
	ClearAllMapNodes(mProc->firstMapPtr);
	mProc->firstMapPtr = NULL;
	mProc->mapCount = 0;
}

int ExcludeThisMap(HWND hList, int sel, int key1, int key2)
{
	NoxMapNode *currentNode = (NoxMapNode *)GetMapNodePtr(hList, sel, key1, key2);
	char filterMapForlder[200] = { 0, };
	int len;

	if (currentNode != NULL)
	{
		GetFilterMapFolder(filterMapForlder, sizeof(filterMapForlder));
		len = strlen(filterMapForlder);
		filterMapForlder[len] = '\\';
		memcpy_s(filterMapForlder + len + 1, (size_t)20, currentNode->mapFileName, (size_t)20);
		if (!rename(currentNode->mapFolderDir, filterMapForlder))
		{
			return (int)currentNode;
		}
	}
	return 0;
}

void GetMapFileNameFromEdit(HWND hEdit, char *dest, int len)
{
	char mMapName[40] = { 0, };
	wchar_t wideMapName[40] = { 0, };

	GetWindowText(hEdit, wideMapName, (int)sizeof(wideMapName));
	WideCharToMultiByte(CP_ACP, 0, wideMapName, sizeof(wideMapName), mMapName, len, NULL, NULL);
	memcpy_s(dest, (size_t)len, mMapName, (size_t)len);
}

void OpenDetailViewDlg(MyMainProc *mProc, HWND hWnd)
{
	int mapSelect = mProc->mapViewSelectIndex;

	if (mapSelect >= 0)
	{
		DialogBox(g_hInst, MAKEINTRESOURCE(IDD_DIALOG3), hWnd, AnalysisDlgProc);
	}
}

void MapfileViewerInit(MyMainProc *mProc)
{
	mProc->typeFilterFlag = 0x1ff;
	KeyRandomInit();
	mProc->mapNodePtr = NULL;
	mProc->mapCount = 0;
	mProc->firstMapPtr = NULL;
	mProc->mapViewSelectIndex = -1;
	sprintf_s(mProc->nullStringData, sizeof(mProc->nullStringData), PickMyStringFromIndex(72));
	memset(mProc->noxDirectory, 0, (size_t)200);
	LoadNoxDirectoryFromFile(PickMyStringFromIndex(71), mProc->noxDirectory);
}

void MapfileViewerExit(MyMainProc *mProc)
{
	ClearNodeArrays(mProc);
	ClearAllMapNodes(mProc->firstMapPtr);
	ClearStringDataFromMemory(mProc);
}

void ClearNodeArrays(MyMainProc *mProc)
{
	if (mProc->mapCount)
	{
		free(mProc->mapNodePtr);
	}
}

void BuildNodeArray(MyMainProc *mProc)
{
	NoxMapNode *curNode = mProc->firstMapPtr;
	int count = mProc->mapCount;

	mProc->mapNodePtr = (int *)malloc(sizeof(int) * count);
	memset(mProc->mapNodePtr, 0, sizeof(int) * count);
	for (int i = count - 1; i >= 0 ; i --)
	{
		mProc->mapNodePtr[i] = (int)curNode;
		curNode = curNode->nextMap;
	}
}

void RebuildNodeArrayWhenDecrease(MyMainProc *mProc, int idx, NoxMapNode *removeNode)
{
	NoxMapNode *curNode = removeNode->prevMap;
	int max = mProc->mapCount;

	if (curNode != NULL)
	{
		for (int i = idx; i < max; i++)
		{
			mProc->mapNodePtr[i] = (int)curNode;
			curNode = curNode->prevMap;
		}
	}
}