#pragma once

#include "mainHead.h"

void DisplayFileInfo(HWND hListView, HWND hEdit, int row, int col);
void WriteMyEditWindow(HWND hEdit, char *content);
int ReadMapFileForGetInfo(char *mapFileName, NoxMapNode *mapNode);
void DisplayAllEmpty(MainFormWnd *mWnd);
void DisplaySelectColumnData(MyMainProc *mProc, int iItem);