
#include "analysisDlg.h"
#include "ctrlDraw.h"
#include "readMapDetail.h"
#include "NoxCryptApi.h"
#include "MapCompress.h"


BOOL CALLBACK AnalysisDlgProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	MyMainProc *mProc = MainProcPtr;

	switch (iMessage)
	{
	case WM_DRAWITEM:
		DrawMyDialogStaticItems((LPDRAWITEMSTRUCT)lParam);
		break;
	case WM_INITDIALOG:
		AnalysisDlgOnInit(hDlg, mProc);
		return 1;
	case WM_COMMAND:
		switch (wParam)
		{
		case ANALYSIS__BUTTON1:
			EndDialog(hDlg, 0);
			break;
		case ANALYSIS__BUTTON2:
			OpenSelectedMapForlder(mProc);
			break;
		case ANALYSIS__BUTTON3:
			SaveDecryptMapFile(mProc, hDlg);
			break;
		case ANALYSIS__BUTTON4:
			SaveEncryptedMapFile(mProc, hDlg);
			break;
		case ANALYSIS__BUTTON5:
			NoxMapCompressionProc(mProc, hDlg);
			break;
		}
		break;
	case WM_CTLCOLORSTATIC:
		if (GetDlgItem(hDlg, ANALYSIS_EDIT) == (HWND)lParam)
			return ListBoxCustomBrush(mProc, (HDC)wParam);
	}
	return 0;
}

void AnalysisDlgSetupFont(HWND hDlg, MainFormWnd *mWnd)
{
	SendMessage(GetDlgItem(hDlg, ANALYSIS__BUTTON1), WM_SETFONT, (WPARAM)mWnd->dlgBFont, 1);
	SendMessage(GetDlgItem(hDlg, ANALYSIS__BUTTON2), WM_SETFONT, (WPARAM)mWnd->dlgBFont, 1);
	SendMessage(GetDlgItem(hDlg, ANALYSIS__BUTTON3), WM_SETFONT, (WPARAM)mWnd->dlgBFont, 1);
	SendMessage(GetDlgItem(hDlg, ANALYSIS__BUTTON4), WM_SETFONT, (WPARAM)mWnd->dlgBFont, 1);
	SendMessage(GetDlgItem(hDlg, ANALYSIS__BUTTON5), WM_SETFONT, (WPARAM)mWnd->dlgBFont, 1);
	SendMessage(GetDlgItem(hDlg, ANALYSIS_STATIC), WM_SETFONT, (WPARAM)mWnd->dlgSFont, 1);
	SendMessage(GetDlgItem(hDlg, ANALYSIS_EDIT), WM_SETFONT, (WPARAM)mWnd->dlgEFont, 1);
}

void AnalysisDlgOnInit(HWND hDlg, MyMainProc *mProc)
{
	wchar_t mapName[20] = { 0, };
	wchar_t wndName[100] = { 0, };
	int selectIndex = mProc->mapViewSelectIndex;
	NoxMapNode *curMapNode;

	if (selectIndex >= 0)
	{
		curMapNode = (NoxMapNode *)mProc->mapNodePtr[selectIndex];
		MyWideCharConvert(curMapNode->mapFileName, (char *)mapName, strlen(curMapNode->mapFileName) + 1);
		wsprintf(wndName, PickMyWideStringFromIndex(114), mapName);
		SetWindowText(hDlg, wndName);
		SetWindowText(GetDlgItem(hDlg, ANALYSIS__BUTTON1), PickMyWideStringFromIndex(115));
		SetWindowText(GetDlgItem(hDlg, ANALYSIS__BUTTON2), PickMyWideStringFromIndex(38));
		SetWindowText(GetDlgItem(hDlg, ANALYSIS__BUTTON3), PickMyWideStringFromIndex(128));
		SetWindowText(GetDlgItem(hDlg, ANALYSIS__BUTTON4), PickMyWideStringFromIndex(129));
		SetWindowText(GetDlgItem(hDlg, ANALYSIS__BUTTON5), PickMyWideStringFromIndex(168)); 
		SetWindowText(GetDlgItem(hDlg, ANALYSIS_STATIC), PickMyWideStringFromIndex(131));
		AnalysisDlgSetupFont(hDlg, mProc->mFormPtr);
		DisplayDetailMapInfo(GetDlgItem(hDlg, ANALYSIS_EDIT), mProc->noxDirectory, curMapNode);
	}
}

void OpenSelectedMapForlder(MyMainProc *mProc)
{
	NoxMapNode *curMap;
	char fullMapDir[0x100] = { 0, };
	int mapSelect = mProc->mapViewSelectIndex;

	if (mapSelect >= 0)
	{
		curMap = (NoxMapNode *)mProc->mapNodePtr[mapSelect];
		sprintf_s(fullMapDir, sizeof(fullMapDir), PickMyStringFromIndex(70), mProc->noxDirectory, curMap->mapFileName);
		WinExec(fullMapDir, SW_SHOW);
	}
}

int DecryptMapStore(char *dStream, int streamSize, char *mapName, int missing, int saveDirNum)
{
	FILE *fp = NULL;
	char saveMap[0x100] = { 0, };

	if (!(_access(PickMyStringFromIndex(saveDirNum), 0) + 1))
		CreateDirectoryA(PickMyStringFromIndex(saveDirNum), NULL);
	sprintf_s(saveMap, sizeof(saveMap), PickMyStringFromIndex(missing), mapName);
	fopen_s(&fp, saveMap, "wb");
	if (fp != NULL)
	{
		fwrite(dStream, (size_t)streamSize, sizeof(char), fp);
		fclose(fp);
		return 1;
	}
	return 0;
}

void SaveDecryptMapFile(MyMainProc *mProc, HWND hDlg)
{
	FILE *fp = NULL;
	char fullMapDir[0x100] = { 0, }, *stream, *dStream;
	int mapSelect = mProc->mapViewSelectIndex, fSize;
	int saveOk = 0;
	NoxMapNode *curMap = (NoxMapNode *)mProc->mapNodePtr[mapSelect];

	sprintf_s(fullMapDir, sizeof(fullMapDir), PickMyStringFromIndex(126), mProc->noxDirectory, curMap->mapFileName, curMap->mapFileName);
	fopen_s(&fp, fullMapDir, "rb");
	if (fp != NULL)
	{
		fSize = GetNoxMapFileSize(fp);
		stream = (char *)malloc(sizeof(char) * fSize);
		fread_s(stream, (size_t)fSize, (size_t)fSize, sizeof(char), fp);
		dStream = NoxAPICrypt(stream, NOX_MAP, fSize, 0);
		free(stream);
		saveOk = DecryptMapStore(dStream, fSize, curMap->mapFileName, 127, 135);
		free(dStream);
		fclose(fp);
	}
	if (saveOk)
		SetWindowText(GetDlgItem(hDlg, ANALYSIS_STATIC), PickMyWideStringFromIndex(132));
	else
		SetWindowText(GetDlgItem(hDlg, ANALYSIS_STATIC), PickMyWideStringFromIndex(133));
}

void SaveEncryptedMapFile(MyMainProc *mProc, HWND hDlg)
{
	FILE *fp = NULL;
	char fullMapDir[0x100] = { 0, }, *stream, *dStream;
	int mapSelect = mProc->mapViewSelectIndex, fSize, saveOk = 0;
	NoxMapNode *curMap = (NoxMapNode *)mProc->mapNodePtr[mapSelect];

	sprintf_s(fullMapDir, sizeof(fullMapDir), PickMyStringFromIndex(127), curMap->mapFileName);
	fopen_s(&fp, fullMapDir, "rb");
	if (fp != NULL)
	{
		fSize = GetNoxMapFileSize(fp);
		dStream = (char *)malloc(sizeof(char) * fSize);
		fread_s(dStream, (size_t)fSize, (size_t)fSize, sizeof(char), fp);
		stream = NoxAPICrypt(dStream, NOX_MAP, fSize, 1);
		free(dStream);
		saveOk = DecryptMapStore(stream, fSize, curMap->mapFileName, 130, 136);
		fclose(fp);
	}
	if (saveOk)
		SetWindowText(GetDlgItem(hDlg, ANALYSIS_STATIC), PickMyWideStringFromIndex(134));
	else
		SetWindowText(GetDlgItem(hDlg, ANALYSIS_STATIC), PickMyWideStringFromIndex(133));
}

void DisplayDetailMapInfo(HWND hEdit, char *noxDir, NoxMapNode *curMapNode)
{
	wchar_t *content = NULL;
	char *stream = NULL;
	char fullPath[256] = { 0, };
	int fSize;
	
	sprintf_s(fullPath, sizeof(fullPath), PickMyStringFromIndex(117), noxDir, curMapNode->mapFileName, curMapNode->mapFileName);
	fSize = ReadNoxMapFileStream(fullPath, &stream);
	if (stream != NULL)
	{
		if (fSize)
		{
			curMapNode->realFileSize = fSize;
			WriteDetailMapInfo(&content, stream, curMapNode);
			if (content != NULL)
			{
				SetWindowText(hEdit, content);
				free(content);
			}
		}
		free(stream);
	}
	else
		SetWindowText(hEdit, PickMyWideStringFromIndex(119));
}

void WriteMultiByteOnMemory(wchar_t *wDest, char *src)
{
	int srcLen = strlen(src);
	wchar_t *wTemp = (short *)malloc(sizeof(short) * srcLen + 1);

	MyWideCharConvert(src, (char *)wTemp, srcLen + 1);
	wTemp[srcLen] = 0;
	wsprintf(wDest + lstrlen(wDest), PickMyWideStringFromIndex(159), wTemp);
	free(wTemp);
}

void AppendWriteOnMemory(wchar_t *wDest, NoxMapForm *mapForm)
{
	wsprintf(wDest, PickMyWideStringFromIndex(116), (wchar_t *)mapForm->mapName); //Write Map name
	wsprintf(wDest + lstrlen(wDest), PickMyWideStringFromIndex(121), mapForm->floorCount); //Write Floor counts
	if (mapForm->edgeCount)
		wsprintf(wDest + lstrlen(wDest), PickMyWideStringFromIndex(137), mapForm->edgeCount); //Write edge counts
	wsprintf(wDest + lstrlen(wDest), PickMyWideStringFromIndex(120), mapForm->wallCount); //Write Wall counts
	wsprintf(wDest + lstrlen(wDest), PickMyWideStringFromIndex(122), mapForm->seWallCount); //Write AutoWall counts
	wsprintf(wDest + lstrlen(wDest), PickMyWideStringFromIndex(123), mapForm->bkWallCount); //Write BreakWall counts
	wsprintf(wDest + lstrlen(wDest), PickMyWideStringFromIndex(125), mapForm->wndWallCount); //Write WindowWall counts
	wsprintf(wDest + lstrlen(wDest), PickMyWideStringFromIndex(124), mapForm->mapWaypoint); //Waypoints
	if (mapForm->groupCount)
		wsprintf(wDest + lstrlen(wDest), PickMyWideStringFromIndex(138), mapForm->groupCount); //Groups
	wsprintf(wDest + lstrlen(wDest), PickMyWideStringFromIndex(142), mapForm->scriptFunc, mapForm->scriptStr); //Groups
	if (mapForm->scriptInfo & 0x10000)
		wsprintf(wDest + lstrlen(wDest), PickMyWideStringFromIndex(145)); //Groups
	if (mapForm->pointCount)
		wsprintf(wDest + lstrlen(wDest), PickMyWideStringFromIndex(148), mapForm->polygonData, mapForm->pointCount); //Groups
	if (mapForm->mapObjectToc)
	{
		wsprintf(wDest + lstrlen(wDest), PickMyWideStringFromIndex(155), mapForm->mapObjectToc); //Groups
		wsprintf(wDest + lstrlen(wDest), PickMyWideStringFromIndex(157), mapForm->mapObjectCount); //Groups
	}
	//if (mapForm->scriptData)
	//	wsprintf(wDest + lstrlen(wDest), PickMyWideStringFromIndex(89), mapForm->scriptData);
	if (mapForm->questTitle != NULL)
		WriteMultiByteOnMemory(wDest, mapForm->questTitle);
	if (mapForm->questImage != NULL)
		WriteMultiByteOnMemory(wDest, mapForm->questImage);
	wsprintf(wDest + lstrlen(wDest), PickMyWideStringFromIndex(206));
}

void WriteDetailMapInfo(wchar_t **wDest, char *stream, NoxMapNode *curMapNode)
{
	NoxMapForm mapForm;
	wchar_t mapName[40] = { 0, };
	wchar_t *wTemp = (short *)malloc(sizeof(short) * 0x200);
	int streamCur = 0x5a2, fSize = curMapNode->realFileSize;
	int *mapFlag = (int *)(stream + streamCur);

	mapForm.questTitle = NULL;
	mapForm.questImage = NULL;
	if (*mapFlag == 2)
	{
		streamCur = 0x5a6;
		GetQuestIntro(stream, &streamCur, &mapForm.questTitle);
		GetQuestIntro(stream, &streamCur, &mapForm.questImage);
	}
	mapForm.wallCount = GetMapWallsCount(stream, &streamCur);

	if (mapForm.wallCount)
	{
		mapForm.floorCount = GetMapTilesCount(stream, &streamCur, &mapForm.edgeCount);
		mapForm.seWallCount = GetMapSecretWallsCount(stream, &streamCur);
		mapForm.bkWallCount = GetMapBreakWallCount(stream, &streamCur);
		mapForm.mapWaypoint = GetMapWaypoints(stream, &streamCur);
		GetMapDebugData(stream, &streamCur);
		mapForm.wndWallCount = GetMapWindowWalls(stream, &streamCur);
		mapForm.groupCount = GetMapGroupCount(stream, &streamCur);
		mapForm.scriptFunc = MapScriptInfo(stream, &streamCur, &mapForm.scriptInfo);
		mapForm.scriptStr = mapForm.scriptInfo & 0xffff;
		MapAmbientData(stream, &streamCur);
		mapForm.polygonData = GetPolygonCount(stream, &streamCur, &mapForm.pointCount);
		MapIntroField(stream, &streamCur);
		mapForm.scriptData = ScriptDataField(stream, &streamCur);
		mapForm.mapObjectToc = ReadMapSectionObjectTOC(stream, &streamCur);
		mapForm.mapObjectCount = ReadMapSectionObjectData(stream, &streamCur, fSize);
	}
	memset(wTemp, 0, (size_t)0x200);
	MyWideCharConvert(curMapNode->mapFileName, (char *)mapName, (size_t)40);
	mapForm.mapName = mapName;
	AppendWriteOnMemory(wTemp, &mapForm);
	if (mapForm.questTitle != NULL)
		free(mapForm.questTitle);
	if (mapForm.questImage != NULL)
		free(mapForm.questImage);
	*wDest = wTemp;
}

void NoxMapCompressionProc(MyMainProc *mProc, HWND hDlg)
{
	NoxMapNode *curMap = (NoxMapNode *)mProc->mapNodePtr[mProc->mapViewSelectIndex];
	char noxMapFile[0x100] = { 0, };
	char compressMap[0x100] = { 0, };

	sprintf_s(noxMapFile, sizeof(noxMapFile), PickMyStringFromIndex(84), mProc->noxDirectory, curMap->mapFileName, curMap->mapFileName);
	sprintf_s(compressMap, sizeof(compressMap), PickMyStringFromIndex(167), mProc->noxDirectory, curMap->mapFileName, curMap->mapFileName);
	if (NoxMapCompressInit(noxMapFile, compressMap))
		SetWindowText(GetDlgItem(hDlg, ANALYSIS_STATIC), PickMyWideStringFromIndex(170));
	else
		SetWindowText(GetDlgItem(hDlg, ANALYSIS_STATIC), PickMyWideStringFromIndex(169));
}