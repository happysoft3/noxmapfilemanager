
#include "loadDir.h"

void GetNoxDirectory(char *dst, char *src)
{
	char *find = strstr(src, "\\");

	if (find != NULL)
		GetNoxDirectory(dst, find + 1);
	else
	{
		strcpy_s(dst, (size_t)20, src);
		memset(src, 0, strlen(dst));
	}
}

int ReadNoxExecFile(char *fn)
{
	FILE *fp = NULL;
	int read, res = 0;

	fopen_s(&fp, fn, PickMyStringFromIndex(78));
	if (fp != NULL)
	{
		fseek(fp, PickMyData(112), 0);
		fread_s(&read, sizeof(int), sizeof(char), sizeof(int), fp);
		if (read == PickMyData(113))
			res = 1;
		fclose(fp);
	}
	return res;
}

void NoxDirectorySaveToFile(char *fileName, char *savedDir)
{
	FILE *fp = NULL;
	int cLen;

	fopen_s(&fp, fileName, PickMyStringFromIndex(77));
	if (fp != NULL)
	{
		cLen = (int)strlen(savedDir);
		fwrite(savedDir, sizeof(char), (size_t)(cLen + 1), fp);
		fclose(fp);
	}
}

void LoadNoxDirectoryFromFile(char *fileName, char *destDir)
{
	FILE *fp = NULL;
	char noxdirectory[200] = { 0, };
	int len;

	fopen_s(&fp, fileName, PickMyStringFromIndex(74));
	if (fp != NULL)
	{
		fgets(noxdirectory, sizeof(noxdirectory), fp);
		len = strlen(noxdirectory);
		sprintf_s(noxdirectory + len, (size_t)10, PickMyStringFromIndex(73));
		if (ReadNoxExecFile(noxdirectory))
		{
			noxdirectory[len] = 0;
			strcpy_s(destDir, sizeof(noxdirectory), noxdirectory);
		}
		fclose(fp);
	}
}

void MyWideCharToMultiChar(wchar_t *wSrc, char *dest)
{
	char temp[0x100] = { 0, };
	int len = WideCharToMultiByte(CP_ACP, 0, wSrc, -1, NULL, 0, NULL, NULL);

	WideCharToMultiByte(CP_ACP, 0, wSrc, -1, temp, len, NULL, NULL);
	memcpy_s(dest, sizeof(temp), temp, sizeof(temp));
}

void SetDefaultSelectPath(char *defPath, wchar_t *wDest)
{
	char pathTemp[0x100] = { 0, };

	while (1)
	{
		if (defPath[0])
		{
			memcpy_s(pathTemp, sizeof(pathTemp), defPath, strlen(defPath) + 1);
			pathTemp[strlen(pathTemp)] = 0;
			if (_access(pathTemp, 0) + 1) //vaild path
			{
				MyWideCharConvert(defPath, (char *)wDest, strlen(defPath) + 1);
				break;
			}
		}
		MyWideCharConvert(PickMyStringFromIndex(182), (char *)wDest, PickMyStringLenFromIndex(182));
		break;
	}
}

static int CALLBACK BrowseCallbackProc(HWND hWnd, UINT uMsg, LPARAM lParam, LPARAM lpData)
{
	wchar_t szPath[0x100] = { 0, };

	switch (uMsg)
	{
	case BFFM_INITIALIZED:
		SendMessage(hWnd, BFFM_SETSELECTION, TRUE, (LPARAM)lpData);
		break;
	case BFFM_SELCHANGED:
		SHGetPathFromIDList((LPCITEMIDLIST)lParam, szPath);
		SendMessage(hWnd, BFFM_SETSTATUSTEXT, 0, (LPARAM)szPath);
		break;
	case BFFM_VALIDATEFAILED:
		MyQuestionBox(hWnd, 183, 184, MB_OK | MB_ICONERROR);
	}
	return 0;
}

int AdvanceLoadNoxDirectory(HWND hWnd, char *dest)
{
	BROWSEINFO bwHwnd;
	wchar_t pszPath[0x100] = { 0, };
	wchar_t szTemp[0x100] = { 0, };
	LPITEMIDLIST pItemIdList;
	char exeFileName[40] = { 0, }, mapPath[0x100] = { 0, };

	SetDefaultSelectPath(dest, pszPath);
	memset(&bwHwnd, 0, sizeof(BROWSEINFO));
	bwHwnd.hwndOwner = hWnd;
	bwHwnd.lpszTitle = PickMyWideStringFromIndex(10);
	bwHwnd.ulFlags = BIF_NEWDIALOGSTYLE | BIF_EDITBOX | BIF_RETURNONLYFSDIRS | BIF_STATUSTEXT | BIF_VALIDATE;
	bwHwnd.lpfn = BrowseCallbackProc;
	bwHwnd.lParam = (LPARAM)pszPath;

	pItemIdList = SHBrowseForFolder(&bwHwnd);
	if (SHGetPathFromIDList(pItemIdList, szTemp))
	{
		MyWideCharToMultiChar(szTemp, mapPath);
		sprintf_s(mapPath + strlen(mapPath), (size_t)(PickMyStringLenFromIndex(73) + 2), "\\%s", PickMyStringFromIndex(73));
		if (mapPath[0] && ReadNoxExecFile(mapPath))
		{
			GetNoxDirectory(exeFileName, mapPath);
			strcpy_s(dest, (size_t)200, mapPath);
			NoxDirectorySaveToFile(PickMyStringFromIndex(76), mapPath);
			return 0;
		}
		else
			return 1;
	}
	return 2;
}

void LoadNoxDirectory(MyMainProc *mProc, HWND hWnd)
{
	if (MyQuestionBox(hWnd, 9, 11, MB_YESNO | MB_ICONQUESTION) == IDYES)
	{
		switch (AdvanceLoadNoxDirectory(hWnd, mProc->noxDirectory))
		{
		case 0: //Success
			break;
		case 1: //Is not game.exe
			MyQuestionBox(hWnd, 13, 14, MB_OK | MB_ICONERROR);
			break;
		case 2: //cancel open dialog
			break;
		}
	}
}