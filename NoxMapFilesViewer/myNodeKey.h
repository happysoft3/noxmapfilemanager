#pragma once

#include "mainHead.h"

int GetMapNodePtr(HWND hListView, int row, int Lkey, int Hkey);

void KeyRandomInit();
int GenerateRandomValue(int min, int max);
void *GetMapNodeKey(int stream);
void StreamEncrypt(char *stream, int lowKey, int highKey);
void StreamDecrypt(char *stream, int lowKey, int highKey);

int GetNodeRealPtr(char *cryptData, char *keyData);
int GetNodeRealPtrOther(char *cryptData, int Lkey, int Hkey);
void NodePtrEncrypt(NoxMapNode *currentNode, char *cryptDestData, int lowKey, int highKey);
char* NodePtrDecrypt(char *cryptData, int lowKey, int highKey);