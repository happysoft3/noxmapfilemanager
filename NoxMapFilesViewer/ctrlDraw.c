
#include "ctrlDraw.h"

void OnDrawItem(HWND hWnd, DRAWITEMSTRUCT *ap_dis)
{
	HDC h_dc = ap_dis->hDC;  // 버튼 윈도우에 그림을 그리기 위한 DC 핸들 값 
	RECT r = ap_dis->rcItem; // 버튼 영역의 좌표 값  

							 // 빨간색 계열의 색상을 사용하여 Brush 객체를 생성한다. 
	HBRUSH h_brush = CreateSolidBrush(RGB(192, 64, 32));
	// 버튼 영역 전체를 빨간색 Brush 객체를 사용하여 사각형으로 채운다. 
	FillRect(h_dc, &r, h_brush);
	// 빨간색 Brush 객체를 제거한다. 
	DeleteObject(h_brush);

	// 어두운 빨간색 Pen 객체를 생성한다. (바깥쪽 테두리에 사용) 
	HPEN h_out_border_pen = CreatePen(PS_SOLID, 1, RGB(64, 0, 0));
	// 밝은 빨간색 Pen 객체를 생성한다. (안쪽 테두리에 사용) 
	HPEN h_in_border_pen = CreatePen(PS_SOLID, 1, RGB(255, 112, 82));
	// 사각형 내부를 채우지 않도록 DC에 NULL_BRUSH를 설정한다. 
	HGDIOBJ h_old_brush = SelectObject(h_dc, GetStockObject(NULL_BRUSH));
	// 바깥쪽 테두리를 만들기 위해서 어두운 빨간색 Pen 객체를 DC 연결한다. 
	HGDIOBJ h_old_pen = SelectObject(h_dc, h_out_border_pen);
	// 내부가 채워지지 않는 사각형을 버튼 크기로 그린다. 
	Rectangle(h_dc, r.left, r.top, r.right, r.bottom);

	if (ap_dis->itemState & ODS_SELECTED) { // 버튼이 눌러진 경우 
											// 버튼이 눌러진 효과를 높이기 위해서 버튼의 Caption을 조금 아래쪽으로 
											// 이동시켜서 출력한다. 
		r.top += 2;
		r.left += 2;
		SetTextColor(h_dc, RGB(255, 255, 0));
	}
	else {
		SetTextColor(h_dc, RGB(255, 255, 200));
		// 밝은 빨간색 Pen 객체를 DC에 연결한다. 
		SelectObject(h_dc, h_in_border_pen);
		// 어두운 빨간색으로 그려진 테두리 안쪽에 다시 테두리를 한 번 더 그린다. 
		Rectangle(h_dc, r.left + 1, r.top + 1, r.right - 1, r.bottom - 1);
	}
	// 이전에 사용하던 Pen 객체를 DC에 다시 연결한다. 
	SelectObject(h_dc, h_old_pen);

	// 밝은 빨간색 Pen 객체를 제거한다. 
	DeleteObject(h_in_border_pen);
	// 어두운 빨간색 Pen 객체를 제거한다. 
	DeleteObject(h_out_border_pen);

	wchar_t name[32];
	// 버튼 컨트롤에서 Caption 정보를 복사한다. 
	int length = GetWindowText(ap_dis->hwndItem, name, 32);
	//int length = GetDlgItemText(ah_dlg, IDC_DEL_BTN, name, 32);

	// 배경 그리기 모드를 투명화 모드로 설정한다. 
	int old_mode = SetBkMode(h_dc, TRANSPARENT);
	//SetTextColor(h_dc, RGB(255, 255, 200));
	// 버튼의 Caption 정보를 버튼 영역의 가운데에 출력한다. 
	DrawText(h_dc, name, length, &r, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
	// 배경 그리기 모드를 기존 모드로 되돌린다. 
	SetBkMode(h_dc, old_mode);
}


void DrawMyDialogStaticItems(LPDRAWITEMSTRUCT lpdis)
{
	wchar_t btnName[100] = { 0, };
	HBRUSH hBrush = CreateSolidBrush(RGB(137, 91, 162));
	HBRUSH oldBrush = (HBRUSH)SelectObject(lpdis->hDC, hBrush);
	RECT rect = lpdis->rcItem;

	//DrawFocusRect(lpdis->hDC, &rect);
	//TODO: rectangle, fill
	FillRect(lpdis->hDC, &rect, hBrush);
	if (lpdis->itemState & ODS_SELECTED)
	{
		rect.top -= 2;
		rect.left -= 2;
		SetTextColor(lpdis->hDC, RGB(255, 255, 128));
		DrawEdge(lpdis->hDC, &rect, EDGE_SUNKEN, BF_RECT);
	}
	else
	{
		SetTextColor(lpdis->hDC, RGB(255, 192, 225));
		DrawEdge(lpdis->hDC, &rect, EDGE_RAISED, BF_RECT);
	}
	SetBkColor(lpdis->hDC, RGB(137, 91, 162));
	GetWindowText(lpdis->hwndItem, btnName, 100);
	DrawText(lpdis->hDC, btnName, lstrlenW(btnName), &rect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
	SelectObject(lpdis->hDC, oldBrush);
	DeleteObject(hBrush);
}

LRESULT ListBoxCustomBrush(MyMainProc *mProc, HDC hdc)
{
	SetTextColor(hdc, RGB(163, 73, 164));
	SetBkColor(hdc, RGB(250, 200, 235));

	return (LRESULT)mProc->mFormPtr->hListBrush;
}

LRESULT StaticCustomBrush(MyMainProc *mProc, HDC hdc)
{
	SetTextColor(hdc, RGB(64, 0, 64));
	SetBkColor(hdc, RGB(255, 255, 255));

	return (LRESULT)mProc->mFormPtr->hStaticBrush;
}