#pragma once

#include <stdio.h>
#include <Windows.h>
#include <CommCtrl.h>
#include <io.h>
#include <ShlObj.h>

#include "resource.h"

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

#define ID_LISTBOX							0
#define ID_MAIN_BUTN1						1
#define ID_MAIN_BUTN2						2
#define ID_MAIN_BUTN3						3
#define ID_MAP_INFO_EDIT1					4
#define ID_MAP_INFO_EDIT2					5
#define ID_MAP_INFO_EDIT3					6
#define ID_MAIN_BUTN4						7
#define ID_MAIN_BUTN5						8
#define ID_MAP_INFO_EDIT4					9
#define ID_MAP_INFO_EDIT5					10
#define ID_MAP_INFO_EDIT6					11
#define ID_MAIN_BUTN6						12


HINSTANCE g_hInst;

typedef struct _map_node
{
	int randomKey;
	int nodeIndex;
	int mapFileSize;
	int realFileSize;
	int Lkey;
	int Rkey;
	int typeFlag;
	char mapFileName[20];
	char mapTitle[0x44];
	char mapVersion[0x14];
	char mapDate[0x24];
	char mapAuthor[0x44];
	char mapTypeName[20];
	char mapFolderDir[200];

	char nodeStream[20];			//Add
	struct _map_node *nextMap;
	struct _map_node *prevMap;
}NoxMapNode;

typedef struct _my_string
{
	int stringLen;
	char *stringData;
}MyStringList;


typedef struct _mainForm_ui
{
	HFONT listFont;
	HFONT butnFont;
	HFONT formFont;
	HFONT drawFont;
	HWND mapList;
	HWND fileNameEdit;
	HWND mapTypeEdit;
	HWND mapTitleEdit;
	HWND mapMakerEdit;
	HWND mapVerEdit;
	HWND mapDateEdit;
	HWND hStatic1;
	HBRUSH hListBrush;
	HBRUSH hStaticBrush;

	HFONT dlgBFont;
	HFONT dlgEFont;
	HFONT dlgSFont;
}MainFormWnd;

typedef struct _my_main_proc
{
	unsigned char existProcErr;
	unsigned char isChangedFilterList;
	int typeFilterFlag;
	int Lkey;
	int Rkey;
	int mapViewSelectIndex;
	int prevSelectIndex;
	int mapCount;
	int allStringCount;
	char nullStringData[8];
	char noxDirectory[200];
	wchar_t retWString[200];
	NoxMapNode *firstMapPtr;
	int *mapNodePtr;
	MyStringList *myStringPic;
	MainFormWnd *mFormPtr;
}MyMainProc;

MyMainProc *MainProcPtr;

#include "MyAppMainForm.h"
#include "MyAppTextUI.h"
#include "patalErrCheck.h"
#include "mapViewProc.h"
#include "hotTools.h"
#include "excludeMapDlg.h"
#include "listViewItem.h"

