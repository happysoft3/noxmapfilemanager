
#include "listviewHandler.h"
#include "ItemOptionHandle.h"

char GetLower(char t)
{
	if (t >= 0x41 && t <= 0x5a)
		return t + 0x20;
	return t;
}

int ListViewKeyMatching(wchar_t *wKey, char *compare)
{
	int len = lstrlen(wKey);
	char key1, key2;

	for (int i = 0; i < len; i++)
	{
		key1 = wKey[i] & 0xff;
		key2 = compare[i];
		if (GetLower(key1) ^ GetLower(key2))
			return 0;
	}
	return 1;
}

LRESULT OnCustomDrawListView(HWND hList, LPARAM lParam)
{
	LPNMLVCUSTOMDRAW lplvcd = (LPNMLVCUSTOMDRAW)lParam;

	switch (lplvcd->nmcd.dwDrawStage) {
	case CDDS_PREPAINT:
		return CDRF_NOTIFYITEMDRAW;
	case CDDS_ITEMPREPAINT:
		if (ListView_GetItemState(hList, lplvcd->nmcd.dwItemSpec, LVIS_FOCUSED | LVIS_SELECTED) == (LVIS_FOCUSED | LVIS_SELECTED))
		{
			int colWidth = 0;
			RECT ItemRect;
			RECT textRect;
			wchar_t iSubTxt[64];
			HRGN bgRgn;
			HBRUSH MyBrush;

			ListView_GetItemRect(hList, lplvcd->nmcd.dwItemSpec, &ItemRect, LVIR_BOUNDS);
			ItemRect.left += 5;
			bgRgn = CreateRectRgnIndirect(&ItemRect);
			MyBrush = CreateSolidBrush(RGB(PickMyData(171), PickMyData(173), PickMyData(175)));
			FillRgn(lplvcd->nmcd.hdc, bgRgn, MyBrush);
			DeleteObject(MyBrush);
			DeleteObject(bgRgn);

			SetTextColor(lplvcd->nmcd.hdc, RGB(PickMyData(172), PickMyData(174), PickMyData(176)));

			for (int i = 0; i < 3; i++)
			{
				if (i) colWidth += ListView_GetColumnWidth(hList, i - 1);
				ListView_GetItemText(hList, lplvcd->nmcd.dwItemSpec, i, iSubTxt, 64);
				SetRect(&textRect, ItemRect.left + colWidth, ItemRect.top, ItemRect.right, ItemRect.bottom);
				DrawText(lplvcd->nmcd.hdc, iSubTxt, -1, &textRect, DT_LEFT | DT_NOCLIP);
			}
			return CDRF_SKIPDEFAULT;
		}
	}
	return CDRF_NOTIFYSUBITEMDRAW;
}

int MapListViewKeyFind(MyMainProc *mProc, LPARAM lParam)
{
	NoxMapNode *mapNode = NULL;
	int pResult = -1;
	LPNMLVFINDITEM pnmfi = NULL;
	pnmfi = (LPNMLVFINDITEM)lParam;
	wchar_t *search = pnmfi->lvfi.psz;
	int startPos = pnmfi->iStart, curPos;

	if (startPos >= mProc->mapCount)
		startPos = 0;
	curPos = startPos;
	do
	{
		mapNode = (NoxMapNode *)mProc->mapNodePtr[curPos];
		if (ListViewKeyMatching(search, mapNode->mapFileName))
		{
			pResult = curPos;
			break;
		}
		curPos = (curPos + 1) % mProc->mapCount;
	} while (curPos ^ startPos);
	return pResult;
}

void OnDispInfoListView(LPARAM lParam, int *nodePtr)
{
	LPNMHDR hdr = (LPNMHDR)lParam;
	NMLVDISPINFO *ndi = (NMLVDISPINFO *)lParam;
	char *picStr = NULL, mapSizeWchar[0x20];
	wchar_t szTmp[100] = { 0, };
	NoxMapNode *mapNode = (NoxMapNode *)nodePtr[ndi->item.iItem];

	switch (ndi->item.iSubItem)
	{
	case 0:
		picStr = mapNode->mapFileName;
		break;
	case 1:
		picStr = mapNode->mapTypeName;
		break;
	case 2:
		sprintf_s(mapSizeWchar, sizeof(mapSizeWchar), PickMyStringFromIndex(85), mapNode->mapFileSize, PickMyStringFromIndex(25));
		picStr = mapSizeWchar;
		break;
	case 3:
		picStr = NULL;
		lstrcpy(szTmp, (wchar_t *)mapNode->nodeStream);
	}
	if (picStr != NULL)
		MyWideCharConvert(picStr, (char *)szTmp, strlen(picStr) + 1);
	lstrcpy(ndi->item.pszText, szTmp);
}

void OnStateChangeListView(void *ptr, HWND hWnd, LPARAM lParam)
{
	NMLISTVIEW *pnmv = (LPNMLISTVIEW)lParam;
	MyMainProc *mProc = ptr;

	if ((pnmv->uNewState ^ pnmv->uOldState) & LVIS_SELECTED)
	{
		DisplaySelectColumnData(mProc, pnmv->iItem);
	}
}

LRESULT MapListViewOnDisplay(HWND hWnd, MyMainProc *mProc, LPARAM lParam)
{
	HWND hList = mProc->mFormPtr->mapList;
	LPNMHDR hdr = (LPNMHDR)lParam;

	if (hdr->hwndFrom == hList)
	{
		switch (hdr->code)
		{
		case LVN_GETDISPINFO:
			OnDispInfoListView(lParam, mProc->mapNodePtr);
			break;
		case NM_CUSTOMDRAW:
			return OnCustomDrawListView(hList, lParam);
		case LVN_ITEMCHANGED:
			OnStateChangeListView(mProc, hWnd, lParam);
			break;
		case LVN_ODFINDITEM:
			return MapListViewKeyFind(mProc, lParam);
		}
	}
	return 0;
}
