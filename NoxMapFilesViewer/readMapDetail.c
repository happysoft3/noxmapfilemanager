
#include "readMapDetail.h"
#include "NoxCryptApi.h"
#include "MapScript.h"
#include "pairTile.h"


int ReadNoxMapFileStream(char *mapFileName, char **destPtr)
{
	FILE *fp = NULL;
	int fSize = 0;
	char *stream = NULL;

	fopen_s(&fp, mapFileName, "rb");
	if (fp != NULL)
	{
		fSize = GetNoxMapFileSize(fp);
		if (fSize)
		{
			stream = (char *)malloc(sizeof(char) * fSize);
			fread_s(stream, (size_t)fSize, (size_t)fSize, sizeof(char), fp);
			*destPtr = NoxAPICrypt(stream, NOX_MAP, fSize, 0);
			free(stream);
		}
		fclose(fp);
		return fSize;
	}
	return -1;
}

int CheckSymbolMatching(char *stream, char *symb, int symbLen)
{
	int matchCount = 0;

	for (int i = symbLen - 1; i >= 0; i--)
	{
		if (stream[i] ^ symb[i])
			break;
		else
			matchCount++;
	}
	return (matchCount == symbLen);
}

int FindSymbolOnStream(char *stream, char *symb, int symbLen, int max)
{
	for (int i = 0; i < max; i++)
	{
		if (CheckSymbolMatching(stream + i, symb, symbLen))
			return i;
	}
	return -1;
}

int FindNonZeroStream(char *stream, int max)
{
	for (int i = 0; i < max; i++)
	{
		if (stream[i])
			return i;
	}
	return -1;
}

int Get8BytePosition(int curPos)
{
	return curPos + (8 - (curPos % 8));
}

int GetQuestIntro(char *stream, int *sCur, char **questIntro)
{
	char *read = NULL;
	char *ptr = stream + *sCur;
	
	if (ptr[0])
	{
		read = (char *)malloc(sizeof(char) * ptr[0] + 1);
		memcpy_s(read, (size_t)ptr[0], ptr + 1, (size_t)ptr[0]);
		read[ptr[0]] = 0;
		*questIntro = read;
		ptr += (ptr[0] + 1);
		*sCur = ptr - stream;
		return 1;
	}
	return 0;
}

int GetMapWallsCount(char *stream, int *sCur)
{
	int symbLen = PickMyStringLenFromIndex(160);
	char *ptr = stream + *sCur;
	int idx = FindSymbolOnStream(ptr, PickMyStringFromIndex(160), symbLen, 0x100);
	int *sectionSize = NULL;

	if (idx + 1)
	{
		ptr = stream + Get8BytePosition(ptr + idx + symbLen - stream);
		sectionSize = (int *)ptr;
		*sCur = (ptr + 8 + *sectionSize - stream);
		return ((*sectionSize) - 19) / 7;
	}
	return -1;
}

int NoxTileSearching(char *stream, int start, int end, int *getEdge)
{
	tPoint pHead;
	int count = 0, cur = start, xyProfile, duple = 0;
	unsigned char xProfile, yProfile, edgeCount;

	pHead.x = 0;
	pHead.pNext = NULL;
	while (cur < end)
	{
		xyProfile = *((int *)(stream + cur));
		yProfile = stream[cur];
		xProfile = stream[cur + 1];
		edgeCount = stream[cur + 7];
		cur += 8;
		if (edgeCount)
		{
			cur += (edgeCount * 5);
			*getEdge += edgeCount;
		}
		if (xProfile & yProfile & 0x80) //TODO: Ȧ����ǥ Ÿ��
		{
			edgeCount = stream[cur + 5];
			AddTilePointList(&pHead, xyProfile, edgeCount);
			cur += 6;
			if (edgeCount)
			{
				cur += (edgeCount * 5);
				*getEdge += edgeCount;
			}
		}
		else //TODO: ¦����ǥ Ÿ��
		{
			if (CheckDuplicateCoor(&pHead, xyProfile))
			{
				duple++;
				*getEdge -= edgeCount;
			}
		}
		count++;
	}
	RemoveAllPointList(pHead.pNext);
	return count + pHead.x - duple;
}

int GetMapTilesCount(char *stream, int *sCur, int *getEdge)
{
	int symbLen = PickMyStringLenFromIndex(161);
	char *ptr = stream + *sCur;
	int idx = FindSymbolOnStream(ptr, PickMyStringFromIndex(161), symbLen, 0x100);
	int res = -1, *sectionSize = NULL;

	if (idx + 1)
	{
		ptr = stream + Get8BytePosition(ptr + idx + symbLen - stream);
		sectionSize = (int *)ptr;
		*sCur = (ptr + 8 + *sectionSize - stream);
		res = NoxTileSearching(stream, ptr + 0x1a - stream, *sCur - 2, getEdge);
	}
	return res;
}

int GetMapSecretWallsCount(char *stream, int *sCur)
{
	int symbLen = PickMyStringLenFromIndex(162);
	char *ptr = stream + *sCur;
	int idx = FindSymbolOnStream(ptr, PickMyStringFromIndex(162), symbLen, 0x100);
	int *sectionSize = NULL, res = 0;

	if (idx + 1)
	{
		ptr = stream + Get8BytePosition(ptr + idx + symbLen - stream);
		sectionSize = (int *)ptr;
		*sCur = (ptr + 8 + *sectionSize - stream);
		return ((*sectionSize) - 4) / 17;
	}
	return -1;
}

int GetMapBreakWallCount(char *stream, int *sCur)
{
	int symbLen = PickMyStringLenFromIndex(163);
	char *ptr = stream + *sCur;
	int idx = FindSymbolOnStream(ptr, PickMyStringFromIndex(163), symbLen, 0x100);
	int *sectionSize = NULL, res = 0;

	if (idx + 1)
	{
		ptr = stream + Get8BytePosition(ptr + idx + symbLen - stream);
		sectionSize = (int *)ptr;
		*sCur = (ptr + 8 + *sectionSize - stream);
		return ((*sectionSize) - 4) / 8;
	}
	return -1;
}

int GetMapWaypoints(char *stream, int *sCur)
{
	int symbLen = PickMyStringLenFromIndex(164);
	char *ptr = stream + *sCur;
	int idx = FindSymbolOnStream(ptr, PickMyStringFromIndex(164), symbLen, 0x100);
	int *sectionSize = NULL;

	if (idx + 1)
	{
		ptr = stream + Get8BytePosition(ptr + idx + symbLen - stream);
		sectionSize = (int *)ptr;
		*sCur = (ptr + 8 + *sectionSize - stream);
		return sectionSize[2] >> 0x10;
	}
	return 0;
}

int GetMapDebugData(char *stream, int *sCur)
{
	int symbLen = PickMyStringLenFromIndex(158);
	char *ptr = stream + *sCur;
	int idx = FindSymbolOnStream(ptr, PickMyStringFromIndex(158), symbLen, 0x100);
	int *sectionSize = NULL;

	if (idx + 1)
	{
		ptr = stream + Get8BytePosition(ptr + idx + symbLen - stream);
		sectionSize = (int *)ptr;
		*sCur = (ptr + 8 + *sectionSize - stream);
		return 1;
	}
	return 0;
}

int GetMapWindowWalls(char *stream, int *sCur)
{
	char *ptr = stream + *sCur;
	int symbLen = PickMyStringLenFromIndex(165);
	int idx = FindSymbolOnStream(ptr, PickMyStringFromIndex(165), symbLen, 0x100);
	int *sectionSize = NULL;

	if (idx + 1)
	{
		ptr = stream + Get8BytePosition(ptr + idx + symbLen - stream);
		sectionSize = (int *)ptr;
		*sCur = (ptr + 8 + *sectionSize - stream);
		return ((*sectionSize) - 4) / 8;
	}
	return 0;
}

int GetMapGroupCount(char *stream, int *sCur)
{
	char *ptr = stream + *sCur;
	int symbLen = PickMyStringLenFromIndex(166);
	int idx = FindSymbolOnStream(ptr, PickMyStringFromIndex(166), symbLen, 0x100);
	int *sectionSize = NULL;

	if (idx + 1)
	{
		ptr = stream + Get8BytePosition(ptr + idx + symbLen - stream);
		sectionSize = (int *)ptr;
		*sCur = (ptr + 8 + *sectionSize - stream);
		return sectionSize[2] >> 0x10;
	}
	return 0;
}

int MapScriptInfo(char *stream, int *sCur, int *strCount)
{
	int symbLen = PickMyStringLenFromIndex(143);
	char *ptr = stream + *sCur;
	int idx = FindSymbolOnStream(ptr, PickMyStringFromIndex(143), symbLen, 0x100), pos = -1, funcCount = -1;
	int *sectionSize = NULL;

	if (idx + 1)
	{
		ptr = stream + Get8BytePosition(ptr + idx + symbLen - stream);
		sectionSize = (int *)ptr;
		*sCur = (ptr + 8 + *sectionSize - stream);
		funcCount = MapScriptStreamEntry(ptr + 0x0e, strCount);
	}
	return funcCount;
}

int MapAmbientData(char *stream, int *sCur)
{
	//why exist this field?
	int symbLen = PickMyStringLenFromIndex(146);
	char *ptr = stream + *sCur;
	int idx = FindSymbolOnStream(ptr, PickMyStringFromIndex(146), symbLen, 0x100);
	int *sectionSize = NULL;

	if (idx + 1)
	{
		ptr = stream + Get8BytePosition(ptr + idx + symbLen - stream);
		sectionSize = (int *)ptr;
		*sCur = (ptr + 8 + *sectionSize - stream);
		return 1;
	}
	return 0;
}

int CalcPolygonPoints(char *stream, int *getPointCount)
{
	char *ptr = stream;
	int *pointCount = (int *)ptr;

	*pointCount = *pointCount >> 0x10;
	ptr += (*pointCount * 0x0c);
	*getPointCount = *pointCount;
	return *((int *)(ptr + 6));
}

int GetPolygonCount(char *stream, int *sCur, int *getPointCount)
{
	char *ptr = stream + *sCur;
	int symbLen = PickMyStringLenFromIndex(147);
	int idx = FindSymbolOnStream(ptr, PickMyStringFromIndex(147), symbLen, 0x100);
	int *sectionSize = NULL;

	if (idx + 1)
	{
		ptr = stream + Get8BytePosition(ptr + idx + symbLen - stream);
		sectionSize = (int *)ptr;
		*sCur = (ptr + 8 + *sectionSize - stream);
		return CalcPolygonPoints(ptr + 0x08, getPointCount);
	}
	return -1;
}

int MapIntroField(char *stream, int *sCur)
{
	char *ptr = stream + *sCur;
	int symbLen = PickMyStringLenFromIndex(149);
	int idx = FindSymbolOnStream(ptr, PickMyStringFromIndex(149), symbLen, 0x100);
	int *sectionSize = NULL;

	if (idx + 1)
	{
		ptr = stream + Get8BytePosition(ptr + idx + symbLen - stream);
		sectionSize = (int *)ptr;
		*sCur = (ptr + 8 + *sectionSize - stream);
		return 1;
	}
	return 0;
}

int ScriptDataField(char *stream, int *sCur)
{
	char *ptr = stream + *sCur;
	int symbLen = PickMyStringLenFromIndex(150);
	int idx = FindSymbolOnStream(ptr, PickMyStringFromIndex(150), symbLen, 0x100);
	int *sectionSize = NULL;

	if (idx + 1)
	{
		ptr = stream + Get8BytePosition(ptr + idx + symbLen - stream);
		sectionSize = (int *)ptr;
		*sCur = (ptr + 8 + *sectionSize - stream);
		//return 1;
		return *sectionSize;
	}
	return 0;
}

int ReadMapSectionObjectTOC(char *stream, int *sCur)
{
	char *ptr = stream + *sCur;
	int symbLen = PickMyStringLenFromIndex(152);
	int idx = FindSymbolOnStream(ptr, PickMyStringFromIndex(152), symbLen, 0x100);
	int *sectionSize = NULL;

	if (idx + 1)
	{
		ptr = stream + Get8BytePosition(ptr + idx + symbLen - stream);
		sectionSize = (int *)ptr;
		*sCur = (ptr + 8 + *sectionSize - stream);
		return sectionSize[2] >> 0x10;
	}
	return -1;
}

int SearchObjectSection(char *read, char *endOffset, char *stream)
{
	char *ptr = read;
	int *size = NULL, count = 0, chkSum;

	while (ptr < endOffset)
	{
		size = (int *)ptr; //3+8+size
		if (*size)
		{
			ptr += (*size + 9);
			chkSum = (8 - ((ptr - stream) % 8));
			//if (chkSum ^ 8)
			ptr += chkSum;
			count++;
		}
		else
			break;
	}
	return count;
}

int ReadMapSectionObjectData(char *stream, int *sCur, int fSize)
{
	char *ptr = stream + *sCur, *endOffset = stream + fSize;
	int symbLen = PickMyStringLenFromIndex(153);
	int idx = FindSymbolOnStream(ptr, PickMyStringFromIndex(153), symbLen, 0x100);
	int *sectionSize = NULL;

	if (idx + 1)
	{
		ptr = stream + Get8BytePosition(ptr + idx + symbLen - stream);
		sectionSize = (int *)ptr;
		*sCur = (ptr + 8 + *sectionSize - stream);
		return SearchObjectSection(ptr + 0x10, endOffset, stream);
	}
	return -1;
}