
#include "excludeMapDlg.h"
#include "ctrlDraw.h"
#include <io.h>
//#include "resource.h"

BOOL CALLBACK InfoDlgProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch (iMessage)
	{
	case WM_DRAWITEM:
		DrawMyDialogStaticItems((LPDRAWITEMSTRUCT)lParam);
		break;
	case WM_INITDIALOG:
		FilterMapListPopup(MainProcPtr, hDlg);
		return 1;
	case WM_CTLCOLORLISTBOX:
		return ListBoxCustomBrush(MainProcPtr, (HDC)wParam);
	case WM_COMMAND:
		switch (wParam)
		{
		/*case IDC_LIST1:
			switch (HIWORD(wParam))
			{
			case LBN_DBLCLK:
			}*/
		case IDC_BUTTON1:					//TODO: Import Maps
			MapComeback(MainProcPtr, hDlg);
			break;
		case IDC_BUTTON2:					//TODO: Just exit this dialog
			EndDialog(hDlg, 0);
			break;
		default:
			break;
		}
	}
	return 0;
}

void FilteredListUpdate(HWND hExList, char *itemName)
{
	wchar_t wItemName[0x14] = { 0, };
	size_t cn;

	mbstowcs_s(&cn, wItemName, 0x20, itemName, 0x20);
	SendMessage(hExList, LB_ADDSTRING, 0, (LPARAM)wItemName);
	
}

int CheckFilterdMapFile(char *mapPath, char *mapFileName)
{
	char mapFileFullName[200] = { 0, };

	sprintf_s(mapFileFullName, sizeof(mapFileFullName), PickMyStringFromIndex(80), mapPath, mapFileName);
	return !_access(mapFileFullName, 0);
}

void GetFilterMapFolder(char *dest, int size)
{
	char filterDir[200] = { 0, };
	int len;

	GetCurrentDirectoryA(size, filterDir);
	len = (int)strlen(filterDir);
	filterDir[len] = '\\';
	sprintf_s(filterDir + len + 1, sizeof(filterDir) - len, PickMyStringFromIndex(37));
	strcpy_s(dest, (size_t)size, filterDir);
}

void LoadFilteringMaps(HWND hExList)
{
	char currentPath[200] = { 0, };
	WIN32_FIND_DATAA fileData;
	HANDLE handle;
	int fullDirLen;

	GetFilterMapFolder(currentPath, (int)sizeof(currentPath));
	sprintf_s(currentPath + strlen(currentPath), (size_t)8, PickMyStringFromIndex(81));
	handle = FindFirstFileA(currentPath, &fileData);
	if (handle == INVALID_HANDLE_VALUE)
	{
		return;
	}
	fullDirLen = strlen(currentPath);
	do
	{
		if (CheckSkipScannigFile(&fileData))
		{
			memcpy_s(currentPath + fullDirLen - 1, strlen(fileData.cFileName) + 1, fileData.cFileName, strlen(fileData.cFileName) + 1);
			if (CheckFilterdMapFile(currentPath, fileData.cFileName))
			{
				FilteredListUpdate(hExList, fileData.cFileName);
			}
		}
	} while (FindNextFileA(handle, &fileData));
	FindClose(handle);
}

void ExMapCountingDisplay(HWND hStatic, HWND hList, wchar_t *wContent)
{
	int mapCount = SendMessage(hList, LB_GETCOUNT, 0, 0);
	wchar_t displayTxt[100] = { 0, };

	wsprintf(displayTxt, wContent, mapCount);
	SetWindowText(hStatic, displayTxt);
}

void FilterMapListPopup(MyMainProc *mProc, HWND hDlg)
{
	mProc->isChangedFilterList = 0;
	SetWindowText(hDlg, PickMyWideStringFromIndex(45));
	SetDlgItemText(hDlg, IDC_BUTTON1, PickMyWideStringFromIndex(35));
	SetDlgItemText(hDlg, IDC_BUTTON2, PickMyWideStringFromIndex(36));
	SetDlgItemText(hDlg, EXMAP_COUNT_TXT, PickMyWideStringFromIndex(65));
	LoadFilteringMaps(GetDlgItem(hDlg, IDC_LIST1));
	ExMapCountingDisplay(GetDlgItem(hDlg, EXMAP_COUNT_TXT), GetDlgItem(hDlg, IDC_LIST1), PickMyWideStringFromIndex(65));
}

void MapComeback(MyMainProc *mProc, HWND hDlg)
{
	HWND hDlgList = GetDlgItem(hDlg, IDC_LIST1);
	int userSelList = SendMessage(hDlgList, LB_GETCURSEL, 0, 0);
	int *checkNoxPath = (int *)mProc->noxDirectory;
	wchar_t wMapFileName[20] = { 0, };
	char mapFileName[20] = { 0, };
	char excludePath[200] = { 0, }, noxMapPath[200] = { 0, };

	if (userSelList >= 0)
	{
		if (*checkNoxPath)
		{
			SendMessage(hDlgList, LB_GETTEXT, (WPARAM)userSelList, (LPARAM)wMapFileName);
			WideCharToMultiByte(CP_ACP, 0, wMapFileName, sizeof(wMapFileName), mapFileName, sizeof(mapFileName), NULL, NULL);
			GetFilterMapFolder(excludePath, sizeof(excludePath));
			sprintf_s(excludePath + strlen(excludePath), sizeof(wMapFileName), PickMyStringFromIndex(82), mapFileName);
			sprintf_s(noxMapPath, sizeof(noxMapPath), PickMyStringFromIndex(83), mProc->noxDirectory, mapFileName);
			if (!rename(excludePath, noxMapPath))
			{
				if (!mProc->isChangedFilterList)
					mProc->isChangedFilterList = 1;
				SendMessage(hDlgList, LB_DELETESTRING, (WPARAM)userSelList, (LPARAM)0);
				ExMapCountingDisplay(GetDlgItem(hDlg, EXMAP_COUNT_TXT), GetDlgItem(hDlg, IDC_LIST1), PickMyWideStringFromIndex(65));
			}
		}
		else
			MyQuestionBox(hDlg, 43, 44, MB_OK | MB_ICONERROR);
	}
}