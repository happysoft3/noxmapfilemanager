
#include "MyAppTextUI.h"
#include "NoxCryptApi.h"

void MoveBmpPixelOffset(FILE *fp)
{
	int offset = 0;

	fseek(fp, 10, SEEK_SET);
	fread_s(&offset, sizeof(int), sizeof(int), (size_t)1, fp);
	fseek(fp, offset, SEEK_SET);
}

void MyWideCharConvert(char *src, char *dest, int len)
{
	wchar_t wStr[200] = { 0, };
	int nLen = MultiByteToWideChar(CP_ACP, 0, src, strlen(src), NULL, 0);

	MultiByteToWideChar(CP_ACP, 0, src, len, wStr, nLen);
	memcpy_s(dest, len * 2, wStr, len * 2 - 2);
}

void GetStringDataFromFile(MyMainProc *pmask, FILE *fp)
{
	int stringChunkLen = 0;

	fread(&pmask->allStringCount, sizeof(int), 1, fp);
	if (pmask->allStringCount)
	{
		pmask->myStringPic = (MyStringList *)malloc(sizeof(MyStringList) * pmask->allStringCount);
		for (int i = 0; i < pmask->allStringCount; i++)
		{
			fread(&stringChunkLen, sizeof(int), 1, fp);
			if (stringChunkLen)
			{
				pmask->myStringPic[i].stringData = (char *)malloc(sizeof(char) * stringChunkLen + 1);
				memset(pmask->myStringPic[i].stringData, 0, (size_t)(stringChunkLen + 1));
				fread_s(pmask->myStringPic[i].stringData, (size_t)stringChunkLen, sizeof(char), (size_t)stringChunkLen, fp);
				pmask->myStringPic[i].stringLen = stringChunkLen;
			}
			else
			{
				pmask->myStringPic[i].stringData = NULL;
				pmask->myStringPic[i].stringLen = 0;
			}
		}
	}
}

void GetStringDataOnMemory(MyMainProc *mProc, char *stream)
{
	char *streamPic = stream + 4;
	int stringChunkLen = 0;

	memcpy_s(&mProc->allStringCount, sizeof(int), stream, sizeof(int));
	if (mProc->allStringCount)
	{
		mProc->myStringPic = (MyStringList *)malloc(sizeof(MyStringList) * mProc->allStringCount);
		for (int i = 0; i < mProc->allStringCount; i++)
		{
			memcpy_s(&stringChunkLen, sizeof(int), streamPic, sizeof(int));
			if (stringChunkLen)
			{
				mProc->myStringPic[i].stringData = (char *)malloc(sizeof(char) * stringChunkLen + 1);
				memset(mProc->myStringPic[i].stringData, 0, (size_t)(stringChunkLen + 1));
				memcpy_s(mProc->myStringPic[i].stringData, (size_t)stringChunkLen, streamPic + 4, (size_t)stringChunkLen);
				mProc->myStringPic[i].stringLen = stringChunkLen;
			}
			else
			{
				mProc->myStringPic[i].stringData = NULL;
				mProc->myStringPic[i].stringLen = 0;
			}
			streamPic += (4 + stringChunkLen);
		}
	}
}

int DecryptBmpFormat(BmpCryptApi *bmpCrypt)
{
	FILE *bmpFp = bmpCrypt->bmpFp;

	MoveBmpPixelOffset(bmpFp);
	fread_s(bmpCrypt, sizeof(BmpCryptApi) - 4, sizeof(int), (size_t)3, bmpFp);
	if (bmpCrypt->bmpHeader == 89430166)
		return 1;
	return 0;
}

int ReadLanguageFile(char *fileName, MyMainProc *mProc)
{
	BmpCryptApi bmpCrypt;
	FILE *fp = NULL;
	char *stream = NULL, *decrypted = NULL;
	int dSize = 0;

	fopen_s(&fp, fileName, "rb");
	if (fp != NULL)
	{
		bmpCrypt.bmpFp = fp;
		if (DecryptBmpFormat(&bmpCrypt))
		{
			dSize = bmpCrypt.dataSize;
			fseek(fp, bmpCrypt.bmpOffset, SEEK_CUR);
			if (dSize && dSize < 0x9000)
			{
				stream = (char *)malloc(sizeof(char) * dSize + 1);
				fread_s(stream, (size_t)dSize, sizeof(char), (size_t)dSize, fp);
				decrypted = NoxAPICrypt(stream, NOX_SAVE, dSize, 0);
				GetStringDataOnMemory(mProc, decrypted);
				free(stream);
				free(decrypted);
			}
			else
				dSize = 0;
		}
		fclose(fp);
	}
	return dSize;
}

void ClearStringDataFromMemory(MyMainProc *pmask)
{
	int max = pmask->allStringCount;

	for (int i = 0; i < max; i++)
	{
		if (pmask->myStringPic[i].stringLen)
			free(pmask->myStringPic[i].stringData);
	}
	free(pmask->myStringPic);
}

char *PickMyStringFromIndex(int index)
{
	MyMainProc *pmask = MainProcPtr;

	if (index < pmask->allStringCount)
	{
		return pmask->myStringPic[index].stringData;
	}
	else
		return pmask->nullStringData;
}

int PickMyStringLenFromIndex(int index)
{
	MyMainProc *pmask = MainProcPtr;

	if (index < pmask->allStringCount)
		return pmask->myStringPic[index].stringLen;
	else
		return 0;
}

wchar_t *PickMyWideStringFromIndex(int index)
{
	MyMainProc *pmask = MainProcPtr;
	char *pickString;
	int pickStringLeng;
	size_t cn;

	if (index < pmask->allStringCount)
	{
		pickString = pmask->myStringPic[index].stringData;
		pickStringLeng = PickMyStringLenFromIndex(index);
		mbstowcs_s(&cn, pmask->retWString, 200, pickString, pickStringLeng);
		MyWideCharConvert(pickString, (char *)pmask->retWString, pickStringLeng);
		return pmask->retWString;
	}
	else
		return (wchar_t *)pmask->nullStringData;
}

int MyQuestionBox(HWND hWnd, int contentIndex, int captionIndex, UINT flag)
{
	wchar_t msgContent[200] = { 0, }, msgCaption[200] = { 0, };

	wsprintf(msgContent, L"%s", PickMyWideStringFromIndex(contentIndex));
	wsprintf(msgCaption, L"%s", PickMyWideStringFromIndex(captionIndex));

	return MessageBox(hWnd, msgContent, msgCaption, flag);
}

int MyQuestionBoxMultiString(HWND hWnd, char *content, int contentSize, char *caption, int captionSize, UINT flag)
{
	wchar_t msgContent[200] = { 0, }, msgCaption[200] = { 0, };

	MyWideCharConvert(content, (char *)msgContent, contentSize);
	MyWideCharConvert(caption, (char *)msgCaption, captionSize);
	wsprintf(msgContent, L"%s", msgContent);
	wsprintf(msgCaption, L"%s", msgCaption);

	return MessageBox(hWnd, msgContent, msgCaption, flag);
}

int PickMyData(int index)
{
	return atoi(PickMyStringFromIndex(index));
}