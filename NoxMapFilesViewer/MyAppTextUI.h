#pragma once

#include "mainHead.h"

//TODO: 언어 UI 파일 가져오기 및 참조 구현입니다

typedef struct _bmpCrypt
{
	int bmpHeader;
	int bmpOffset;
	int dataSize;
	FILE *bmpFp;
}BmpCryptApi;

void MoveBmpPixelOffset(FILE *fp);

void MyWideCharConvert(char *src, char *dest, int len);

void GetStringDataFromFile(MyMainProc *mProc, FILE *fp);
void GetStringDataOnMemory(MyMainProc *mProc, char *stream);
int DecryptBmpFormat(BmpCryptApi *bmpCrypt);
int ReadLanguageFile(char *fileName, MyMainProc *mProc);
void ClearStringDataFromMemory(MyMainProc *mProc);

char *PickMyStringFromIndex(int index);
int PickMyStringLenFromIndex(int index);
wchar_t *PickMyWideStringFromIndex(int index);
int MyQuestionBox(HWND hWnd, int contentIndex, int captionIndex, UINT flag);
int MyQuestionBoxMultiString(HWND hWnd, char *content, int contentSize, char *caption, int captionSize, UINT flag);

int PickMyData(int index);