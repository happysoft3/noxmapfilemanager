#pragma once

#include "mainHead.h"


BOOL CALLBACK InfoDlgProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam);

void FilteredListUpdate(HWND hExList, char *itemName);
int CheckFilterdMapFile(char *mapPath, char *mapFileName);
void GetFilterMapFolder(char *dest, int size);
void LoadFilteringMaps(HWND hExList);
void ExMapCountingDisplay(HWND hStatic, HWND hList, wchar_t *wContent);
void FilterMapListPopup(MyMainProc *mProc, HWND hDlg);
void MapComeback(MyMainProc *mProc, HWND hDlg);