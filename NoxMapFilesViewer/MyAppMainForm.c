
#include "listViewItem.h"
#include "MyAppMainForm.h"
#include "filterEditDlg.h"


void MyListViewMakeColumn(HWND hLstView)
{
	LVCOLUMN ListCol;

	ListView_SetExtendedListViewStyle(hLstView, LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES); //LVS_EX_DOUBLEBUFFER
	ListCol.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
	ListCol.fmt = LVCFMT_LEFT;

	ListCol.cx = PickMyData(90);
	ListCol.pszText = PickMyWideStringFromIndex(4); //File name
	ListCol.iSubItem = 0;
	ListView_InsertColumn(hLstView, 0, &ListCol);

	ListCol.cx = PickMyData(91);
	ListCol.pszText = PickMyWideStringFromIndex(5);	//Map Type
	ListCol.iSubItem = 1;
	ListView_InsertColumn(hLstView, 1, &ListCol);

	ListCol.cx = PickMyData(92);
	ListCol.pszText = PickMyWideStringFromIndex(6); //Map Size
	ListCol.iSubItem = 2;
	ListView_InsertColumn(hLstView, 2, &ListCol);

	ListCol.cx = PickMyData(93);
	ListCol.pszText = PickMyWideStringFromIndex(33); //Link Ptr
	ListCol.iSubItem = 3;
	ListView_InsertColumn(hLstView, 3, &ListCol);
}

void MainFormOnCreateHandler(MyMainProc *mProc, HWND hWnd)
{
	HWND excludeMapButn;
	HWND selMapListUpdate;
	HWND selNoxPathButn;
	HWND showExMapButn;
	HWND openMapDirButn;
	HWND openFilterEditButn;
	HWND caption1, caption2, caption3, caption4, caption5, caption6;
	MainFormWnd *mainWnd = mProc->mFormPtr;
	int butnWidth = PickMyData(109), butnHeight = PickMyData(110);

	mainWnd->butnFont = CreateFont(PickMyData(94), 0, 0, 0, 0, 0, 0, 0, HANGEUL_CHARSET, 3, 2, 1, VARIABLE_PITCH | FF_ROMAN, PickMyWideStringFromIndex(67));
	mainWnd->listFont = CreateFont(PickMyData(95), 0, 0, 0, 0, 0, 0, 0, HANGEUL_CHARSET, 3, 2, 1, VARIABLE_PITCH | FF_ROMAN, PickMyWideStringFromIndex(68));
	mainWnd->formFont = CreateFont(PickMyData(96), 0, 0, 0, 0, 0, 0, 0, HANGEUL_CHARSET, 3, 2, 1, VARIABLE_PITCH | FF_ROMAN, PickMyWideStringFromIndex(69));
	mainWnd->drawFont = CreateFont(PickMyData(181), 0, 0, 0, 0, 0, 0, 0, HANGEUL_CHARSET, 3, 2, 1, VARIABLE_PITCH | FF_ROMAN, PickMyWideStringFromIndex(180));

	mainWnd->dlgBFont = CreateFont(PickMyData(187), 0, 0, 0, 0, 0, 0, 0, HANGEUL_CHARSET, 3, 2, 1, VARIABLE_PITCH | FF_ROMAN, PickMyWideStringFromIndex(189));
	mainWnd->dlgEFont = CreateFont(PickMyData(199), 0, 0, 0, 0, 0, 0, 0, HANGEUL_CHARSET, 3, 2, 1, VARIABLE_PITCH | FF_ROMAN, PickMyWideStringFromIndex(195));
	mainWnd->dlgSFont = CreateFont(PickMyData(208), 0, 0, 0, 0, 0, 0, 0, HANGEUL_CHARSET, 3, 2, 1, VARIABLE_PITCH | FF_ROMAN, PickMyWideStringFromIndex(202));

	mainWnd->mapList = CreateWindow(WC_LISTVIEW, NULL, WS_VISIBLE | WS_CHILD | WS_BORDER | LVS_REPORT | LVS_SINGLESEL | LVS_OWNERDATA,
		10, 10, 280, 330, hWnd, (HMENU)ID_LISTBOX, g_hInst, NULL);

	selNoxPathButn = CreateWindow(WC_BUTTON, PickMyWideStringFromIndex(8), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW,
		320, 240, butnWidth, butnHeight, hWnd, (HMENU)ID_MAIN_BUTN1, g_hInst, NULL);
	selMapListUpdate = CreateWindow(WC_BUTTON, PickMyWideStringFromIndex(15), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW,
		320, 270, butnWidth, butnHeight, hWnd, (HMENU)ID_MAIN_BUTN2, g_hInst, NULL);
	excludeMapButn = CreateWindow(WC_BUTTON, PickMyWideStringFromIndex(29), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW,
		320, 210, butnWidth, butnHeight, hWnd, (HMENU)ID_MAIN_BUTN3, g_hInst, NULL);
	showExMapButn = CreateWindow(WC_BUTTON, PickMyWideStringFromIndex(34), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW,
		440, 240, butnWidth, butnHeight, hWnd, (HMENU)ID_MAIN_BUTN4, g_hInst, NULL);
	openMapDirButn = CreateWindow(WC_BUTTON, PickMyWideStringFromIndex(118), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW,
		440, 210, butnWidth, butnHeight, hWnd, (HMENU)ID_MAIN_BUTN5, g_hInst, NULL);
	openFilterEditButn = CreateWindow(WC_BUTTON, PickMyWideStringFromIndex(49), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW,
		440, 270, butnWidth, butnHeight, hWnd, (HMENU)ID_MAIN_BUTN6, g_hInst, NULL);
	
	caption1 = CreateWindow(WC_STATIC, PickMyWideStringFromIndex(30), WS_CHILD | WS_VISIBLE,
		320, 10, 80, 20, hWnd, (HMENU)-1, g_hInst, NULL);
	mainWnd->fileNameEdit = CreateWindow(WC_EDIT, NULL, WS_CHILD | WS_VISIBLE | WS_BORDER | ES_READONLY | ES_MULTILINE | WS_DISABLED,
		410, 10, 130, 20, hWnd, (HMENU)ID_MAP_INFO_EDIT1, g_hInst, NULL);

	caption2 = CreateWindow(WC_STATIC, PickMyWideStringFromIndex(31), WS_CHILD | WS_VISIBLE,
		320, 35, 80, 20, hWnd, (HMENU)-1, g_hInst, NULL);
	mainWnd->mapTypeEdit = CreateWindow(WC_EDIT, NULL, WS_CHILD | WS_VISIBLE | WS_BORDER | ES_READONLY | ES_MULTILINE | WS_DISABLED,
		410, 35, 130, 20, hWnd, (HMENU)ID_MAP_INFO_EDIT2, g_hInst, NULL);

	caption3 = CreateWindow(WC_STATIC, PickMyWideStringFromIndex(32), WS_CHILD | WS_VISIBLE,
		320, 60, 80, 20, hWnd, (HMENU)-1, g_hInst, NULL);
	mainWnd->mapTitleEdit = CreateWindow(WC_EDIT, NULL, WS_CHILD | WS_VISIBLE | WS_BORDER | ES_READONLY | ES_MULTILINE | WS_DISABLED,
		410, 60, 130, 40, hWnd, (HMENU)ID_MAP_INFO_EDIT3, g_hInst, NULL);

	caption4 = CreateWindow(WC_STATIC, PickMyWideStringFromIndex(46), WS_CHILD | WS_VISIBLE,
		320, 105, 80, 20, hWnd, (HMENU)-1, g_hInst, NULL);
	mainWnd->mapMakerEdit = CreateWindow(WC_EDIT, NULL, WS_CHILD | WS_VISIBLE | WS_BORDER | ES_READONLY | ES_MULTILINE | WS_DISABLED,
		410, 105, 130, 20, hWnd, (HMENU)ID_MAP_INFO_EDIT4, g_hInst, NULL);

	caption5 = CreateWindow(WC_STATIC, PickMyWideStringFromIndex(47), WS_CHILD | WS_VISIBLE,
		320, 130, 80, 20, hWnd, (HMENU)-1, g_hInst, NULL);
	mainWnd->mapVerEdit = CreateWindow(WC_EDIT, NULL, WS_CHILD | WS_VISIBLE | WS_BORDER | ES_READONLY | ES_MULTILINE | WS_DISABLED,
		410, 130, 130, 20, hWnd, (HMENU)ID_MAP_INFO_EDIT5, g_hInst, NULL);

	caption6 = CreateWindow(WC_STATIC, PickMyWideStringFromIndex(48), WS_CHILD | WS_VISIBLE,
		320, 155, 80, 20, hWnd, (HMENU)-1, g_hInst, NULL);
	mainWnd->mapDateEdit = CreateWindow(WC_EDIT, NULL, WS_CHILD | WS_VISIBLE | WS_BORDER | ES_READONLY | ES_MULTILINE | WS_DISABLED,
		410, 155, 130, 20, hWnd, (HMENU)ID_MAP_INFO_EDIT6, g_hInst, NULL);

	mainWnd->hStatic1 = CreateWindow(WC_STATIC, L"", WS_CHILD | WS_VISIBLE,
		PickMyData(103), PickMyData(104), PickMyData(105), PickMyData(106), hWnd, (HMENU)-1, g_hInst, NULL);

	SendMessage(mainWnd->mapList, WM_SETFONT, (WPARAM)mainWnd->listFont, 1);
	SendMessage(selNoxPathButn, WM_SETFONT, (WPARAM)mainWnd->butnFont, 1);
	SendMessage(selMapListUpdate, WM_SETFONT, (WPARAM)mainWnd->butnFont, 1);
	SendMessage(excludeMapButn, WM_SETFONT, (WPARAM)mainWnd->butnFont, 1);
	SendMessage(showExMapButn, WM_SETFONT, (WPARAM)mainWnd->butnFont, 1);
	SendMessage(openMapDirButn, WM_SETFONT, (WPARAM)mainWnd->butnFont, 1);
	SendMessage(openFilterEditButn, WM_SETFONT, (WPARAM)mainWnd->butnFont, 1);

	SendMessage(caption1, WM_SETFONT, (WPARAM)mainWnd->formFont, 1);
	SendMessage(caption2, WM_SETFONT, (WPARAM)mainWnd->formFont, 1);
	SendMessage(caption3, WM_SETFONT, (WPARAM)mainWnd->formFont, 1);
	SendMessage(caption4, WM_SETFONT, (WPARAM)mainWnd->formFont, 1);
	SendMessage(caption5, WM_SETFONT, (WPARAM)mainWnd->formFont, 1);
	SendMessage(caption6, WM_SETFONT, (WPARAM)mainWnd->formFont, 1);
	SendMessage(mainWnd->hStatic1, WM_SETFONT, (WPARAM)mainWnd->formFont, 1);
	SendMessage(mainWnd->fileNameEdit, WM_SETFONT, (WPARAM)mainWnd->formFont, 1);
	SendMessage(mainWnd->mapTypeEdit, WM_SETFONT, (WPARAM)mainWnd->formFont, 1);
	SendMessage(mainWnd->mapTitleEdit, WM_SETFONT, (WPARAM)mainWnd->formFont, 1);
	SendMessage(mainWnd->mapMakerEdit, WM_SETFONT, (WPARAM)mainWnd->formFont, 1);
	SendMessage(mainWnd->mapVerEdit, WM_SETFONT, (WPARAM)mainWnd->formFont, 1);
	SendMessage(mainWnd->mapDateEdit, WM_SETFONT, (WPARAM)mainWnd->formFont, 1);

	//TODO: 리스트 브러쉬
	mainWnd->hListBrush = CreateSolidBrush(RGB(PickMyData(97), PickMyData(98), PickMyData(99)));
	//TODO: 스태틱 브러쉬
	mainWnd->hStaticBrush = CreateSolidBrush(RGB(PickMyData(100), PickMyData(101), PickMyData(102)));
	//TODO: 리스트뷰 스타일 초기화
	MyListViewMakeColumn(mainWnd->mapList);
}

void DrawMapCounting(HWND hWnd)
{
	RECT drawRect;

	SetRect(&drawRect, 320, 310, 460, 340);
	InvalidateRect(hWnd, &drawRect, 1);
}

void DisplayMapsCount(MyMainProc *mProc, HDC *hdc)
{
	wchar_t wstr[80] = { 0, };

	SetTextColor(*hdc, RGB(PickMyData(177), PickMyData(178), PickMyData(179)));
	wsprintf(wstr, PickMyWideStringFromIndex(28), mProc->mapCount);
	TextOut(*hdc, 320, 310, wstr, lstrlen(wstr));
}

void MainFormOnDrawHandler(MyMainProc *mProc, HWND hWnd)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	HFONT org_font = (HFONT)SelectObject(hdc, mProc->mFormPtr->drawFont);

	DisplayMapsCount(mProc, &hdc);

	SelectObject(hdc, org_font);
	EndPaint(hWnd, &ps);
}

void AddMapListViewItem(HWND hMapList, int row, int col, char *itemName)
{
	wchar_t wideItemName[100] = { 0, };
	LVITEM lv;

	lv.iItem = row;
	lv.iSubItem = col;
	MyWideCharConvert(itemName, (char *)wideItemName, strlen(itemName) + 1);
	lv.pszText = wideItemName;
	lv.mask = LVFIF_TEXT;
	if (!col)
		ListView_InsertItem(hMapList, &lv);
	else
		ListView_SetItem(hMapList, &lv);
}

void AddMapNodeAddressItem(HWND hMapList, int row, int col, char *itemName) //not used
{
	LVITEM lv;

	lv.iItem = row;
	lv.iSubItem = col;
	lv.pszText = (wchar_t *)itemName;
	lv.mask = LVFIF_TEXT;
	if (!col)
		ListView_InsertItem(hMapList, &lv);
	else
		ListView_SetItem(hMapList, &lv);
}

void ArrangeMapsOnList(NoxMapNode *currentNode, HWND mapListWnd)
{
	char mapSizeWchar[0x20];

	if (currentNode != NULL)
	{
		ArrangeMapsOnList(currentNode->nextMap, mapListWnd);
		AddMapListViewItem(mapListWnd, currentNode->nodeIndex, 0, currentNode->mapFileName);
		AddMapListViewItem(mapListWnd, currentNode->nodeIndex, 1, currentNode->mapTypeName);
		sprintf_s(mapSizeWchar, sizeof(mapSizeWchar), "%d %s", currentNode->mapFileSize, PickMyStringFromIndex(25));
		AddMapListViewItem(mapListWnd, currentNode->nodeIndex, 2, mapSizeWchar);
		AddMapNodeAddressItem(mapListWnd, currentNode->nodeIndex, 3, currentNode->nodeStream);		//Move 6 Oct 2019
	}
}

void UpdateMapsListView(MyMainProc *mProc, HWND hWnd)
{
	int *checkNoxPath = (int *)mProc->noxDirectory;
	HWND mapListWnd = mProc->mFormPtr->mapList;

	SetWindowText(mProc->mFormPtr->hStatic1, PickMyWideStringFromIndex(66));
	SendMessage(mapListWnd, WM_SETREDRAW, 0, 0);
	ListView_DeleteAllItems(mapListWnd);
	//ListView_SetItemCount(mapListWnd, 0);
	ClearNodeArrays(mProc);
	if (*checkNoxPath)
	{
		mProc->mapViewSelectIndex = -1;
		MakeMapNodeList(mProc);
		if (mProc->mapCount)
		{
			BuildNodeArray(mProc); //new
			ListView_SetItemCount(mapListWnd, mProc->mapCount);
			DrawMapCounting(hWnd);
		}
		else
			MyQuestionBox(hWnd, 27, 3, MB_OK | MB_ICONERROR);
	}
	else
		MyQuestionBox(hWnd, 43, 44, MB_OK | MB_ICONERROR);
	SendMessage(mapListWnd, WM_SETREDRAW, 1, 0);
	SetWindowText(mProc->mFormPtr->hStatic1, L"");
}

void NextMapFocusOnList(HWND hList, int *cursel, int ItemCounts)
{
	if (ItemCounts && *cursel >= 0)
	{
		if (*cursel >= ItemCounts)
			*cursel = ItemCounts - 1;
		ListView_SetItemState(hList, *cursel, LVNI_SELECTED | LVNI_FOCUSED, LVNI_SELECTED | LVNI_FOCUSED);
		ListView_EnsureVisible(hList, *cursel, 0);
		SetFocus(hList);
	}
}

void ExcludeSelectedMap(MyMainProc *mProc, HWND hWnd)
{
	int selectList = mProc->mapViewSelectIndex;
	char filterMapDir[200] = { 0, };
	char successMent[200] = { 0, };
	HWND hList = mProc->mFormPtr->mapList;
	NoxMapNode *removeNode = NULL;

	if (selectList >= 0)
	{
		if (MyQuestionBox(hWnd, 39, 40, MB_OKCANCEL | MB_ICONQUESTION) == IDOK)
		{
			removeNode = (NoxMapNode *)ExcludeThisMap(mProc->mFormPtr->mapList, selectList, mProc->Lkey, mProc->Rkey);
			if (removeNode != NULL)
			{
				mProc->mapViewSelectIndex = -1;
				mProc->mapCount -= 1;
				GetFilterMapFolder(filterMapDir, sizeof(filterMapDir));
				sprintf_s(successMent, sizeof(successMent), PickMyStringFromIndex(41), filterMapDir);
				RebuildNodeArrayWhenDecrease(mProc, selectList, removeNode);
				RemoveCurrentNode(&mProc->firstMapPtr, removeNode);
				if (mProc->mapCount)
					ListView_DeleteItem(hList, selectList);
				else
					ListView_DeleteAllItems(hList);
				MyQuestionBoxMultiString(hWnd, successMent, sizeof(successMent), successMent, sizeof(successMent), MB_OK | MB_ICONINFORMATION);
				NextMapFocusOnList(hList, &selectList, mProc->mapCount);
				DrawMapCounting(hWnd);
			}
			else
				MyQuestionBox(hWnd, 42, 3, MB_OK | MB_ICONERROR);
		}
	}
}

void ShowDlgFilteredMapsList(MyMainProc *mProc, HWND hWnd)
{
	int *checkLoadDir = (int *)mProc->noxDirectory;

	if (*checkLoadDir)
	{
		DialogBox(g_hInst, MAKEINTRESOURCE(IDD_DIALOG1), hWnd, InfoDlgProc);

		if (mProc->isChangedFilterList)
		{
			UpdateMapsListView(mProc, hWnd);
			NextMapFocusOnList(mProc->mFormPtr->mapList, &mProc->prevSelectIndex, mProc->mapCount);
		}
	}
}

void ShowFilterEditorDialog(MyMainProc *mProc, HWND hWnd)
{
	int *checkLoadDir = (int *)mProc->noxDirectory;
	int prevFlag = mProc->typeFilterFlag;

	if (*checkLoadDir)
	{
		DialogBox(g_hInst, MAKEINTRESOURCE(IDD_DIALOG2), hWnd, FilterEditDlgProc);
		if (prevFlag ^ mProc->typeFilterFlag)
		{
			UpdateMapsListView(mProc, hWnd);
			NextMapFocusOnList(mProc->mFormPtr->mapList, &mProc->prevSelectIndex, mProc->mapCount);
		}
	}
}

void MainFormOnClose(MyMainProc *mProc)
{
	MainFormWnd *mainWnd = mProc->mFormPtr;

	DeleteObject(mainWnd->butnFont);
	DeleteObject(mainWnd->listFont);
	DeleteObject(mainWnd->formFont);
	DeleteObject(mainWnd->hListBrush);
	DeleteObject(mainWnd->hStaticBrush);
	DeleteObject(mainWnd->drawFont);

	DeleteObject(mainWnd->dlgBFont);
	DeleteObject(mainWnd->dlgEFont);
	DeleteObject(mainWnd->dlgSFont);
}