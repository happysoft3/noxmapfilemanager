#pragma once

#include "mainHead.h"

void GetMapFolderName(char *fullDir, char *dest, int destSize);
int GetNoxMapFileSize(FILE *fp);
int GetNoxMapType(FILE *fp);
void GetMapTitleInfo(FILE *fp, char *title);
void GetMapDetailInfo(FILE *fp, char *destInfo, int destLen, int offset);
int InsertMapTypeName(char *destStr, int typeFlag, int destStrLen);
int GetMapFileInfo(NoxMapNode *currentMap, char *fullDirName, int filterFlag);
int MapFileScan(NoxMapNode **pic, int **argList, int filterFlag);

void ScanTargetDirectory(MyMainProc *mProc, char *targDir);
void MakeMapNodeList(MyMainProc *mProc);

void RemoveCurrentNode(NoxMapNode **firstNode, NoxMapNode *currentNode);