#pragma once

#include "mainHead.h"

typedef struct _noxMapFormat
{
	short *mapName;
	int wallCount;
	int seWallCount;
	int wndWallCount;
	int bkWallCount;
	int floorCount;
	int edgeCount;
	int mapWaypoint;
	int mapObjectToc;
	int mapObjectCount;
	int groupCount;
	int polygonData;
	int pointCount;
	int scriptFunc;
	int scriptInfo;
	int scriptStr;
	char *questTitle;
	char *questImage;
	int scriptData;
}NoxMapForm;

BOOL CALLBACK AnalysisDlgProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam);
void AnalysisDlgOnInit(HWND hDlg, MyMainProc *mProc);
void OpenSelectedMapForlder(MyMainProc *mProc);
void SaveDecryptMapFile(MyMainProc *mProc, HWND hDlg);
void SaveEncryptedMapFile(MyMainProc *mProc, HWND hDlg);
void DisplayDetailMapInfo(HWND hEdit, char *noxDir, NoxMapNode *curMapNode);
void WriteDetailMapInfo(wchar_t **wDest, char *stream, NoxMapNode *curMapNode);
void NoxMapCompressionProc(MyMainProc *mProc, HWND hDlg);



