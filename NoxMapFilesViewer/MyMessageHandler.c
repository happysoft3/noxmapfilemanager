
#include "mainHead.h"
#include "ctrlDraw.h"
#include "loadDir.h"
#include "ItemOptionHandle.h"
#include "listviewHandler.h"


LRESULT CALLBACK WndProc(HWND hWnd, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	MyMainProc *mProc = MainProcPtr;

	switch (iMessage)
	{
	case WM_CREATE:
		MainFormOnCreateHandler(mProc, hWnd);
		break;
	case WM_DESTROY:
		MapfileViewerExit(mProc);
		PostQuitMessage(0);
		return 0;
	case WM_PAINT:
		MainFormOnDrawHandler(mProc, hWnd);
		break;
	case WM_CLOSE:
		MainFormOnClose(mProc);
		break;
	case WM_DRAWITEM:
		OnDrawItem(hWnd, (DRAWITEMSTRUCT *)lParam);
		break;
	case WM_NOTIFY:
		return MapListViewOnDisplay(hWnd, mProc, lParam);
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case ID_MAIN_BUTN1:				//TODO: 녹스 디렉토리 가져오기
			LoadNoxDirectory(mProc, hWnd);
			break;
		case ID_MAIN_BUTN2:				//TODO: 맵 리스트 업데이트
			UpdateMapsListView(mProc, hWnd);
			DisplayAllEmpty(mProc->mFormPtr);
			break;
		case ID_MAIN_BUTN3:				//TODO: 선택된 맵 제외
			ExcludeSelectedMap(mProc, hWnd);
			break;
		case ID_MAIN_BUTN4:
			ShowDlgFilteredMapsList(mProc, hWnd);
			break;
		case ID_MAIN_BUTN5:
			OpenDetailViewDlg(mProc, hWnd);
			break;
		case ID_MAIN_BUTN6:
			ShowFilterEditorDialog(mProc, hWnd);
		}
		break;
	case WM_CTLCOLORSTATIC:
		return StaticCustomBrush(mProc, (HDC)wParam);
	}
	return (DefWindowProc(hWnd, iMessage, wParam, lParam));
}