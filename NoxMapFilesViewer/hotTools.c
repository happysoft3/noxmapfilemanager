
#include "NoxCryptApi.h"
#include "hotTools.h"

int CheckSkipScannigFile(void *ptr)
{
	WIN32_FIND_DATAA *fileData = ptr;

	if (!strcmp(fileData->cFileName, ".") || !strcmp(fileData->cFileName, ".."))	//TODO: Skip to dir named . or ..
		return 0;
	if (fileData->dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		return 1;
	else
		return 0;
}


void TestPrintMessage(HWND hWnd, int val, char *str)
{
	wchar_t wstr[200] = { 0, };
	size_t cn;

	if (str == NULL)
	{
		wsprintf(wstr, PickMyWideStringFromIndex(89), val);
	}
	else
		mbstowcs_s(&cn, wstr, sizeof(wstr), str, strlen(str));
	MessageBox(hWnd, wstr, PickMyWideStringFromIndex(0), MB_OK | MB_ICONINFORMATION);
}