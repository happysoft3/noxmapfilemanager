
#include "pairTile.h"

void RemoveDupleNode(tPoint *pDelNode)
{
	pDelNode->pPrev->pNext = pDelNode->pNext;
	if (pDelNode->pNext != NULL)
		pDelNode->pNext->pPrev = pDelNode->pPrev;
	free(pDelNode);
}

int CheckDuplicateCoor(tPoint *pHead, int xyProfile)
{
	int xProfile = ((xyProfile >> 0x08) & 0x7f) * 2, yProfile = (xyProfile & 0x7f) * 2;
	tPoint *tCur = pHead->pNext;

	while (tCur != NULL)
	{
		if (xProfile == tCur->x && yProfile == tCur->y)
		{
			RemoveDupleNode(tCur);
			return 1;
		}
		tCur = tCur->pNext;
	}
	return 0;
}

void AddTilePointList(tPoint *pHead, int xyProfile, int edgeCount)
{
	int xProfile = ((xyProfile >> 0x08) & 0x7f) * 2, yProfile = (xyProfile & 0x7f) * 2;
	tPoint *pNode = (tPoint *)malloc(sizeof(tPoint));

	pNode->x = xProfile;
	pNode->y = yProfile;
	pNode->edge = edgeCount & 0xff;
	pNode->pNext = pHead->pNext;
	if (pHead->pNext != NULL)
		pHead->pNext->pPrev = pNode;
	pNode->pPrev = pHead;
	pHead->pNext = pNode;
	pHead->x++;
}

void RemoveAllPointList(tPoint *tCur)
{
	if (tCur != NULL)
	{
		RemoveAllPointList(tCur->pNext);
		free(tCur);
	}
}