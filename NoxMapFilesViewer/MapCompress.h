#pragma once

#include "mainHead.h"
#include "readMapDetail.h"

int BitWriteOnMemory(char *stream, char *output, int *bitWrite, int len);
int NoxMapCompressMain(char *stream, char *output);
int NoxMapCompressInit(char *fName, char *outfName);
