
#include "MapCompress.h"
#include "pairTile.h"
#include "NoxCryptApi.h"
#include "polygonFix.h"


int BitWriteOnMemory(char *stream, char *output, int *bitWrite, int len)
{
	char *ptr = output + *bitWrite;

	for (int i = 0; i < len; i++)
		ptr[i] = stream[i];
	*bitWrite += len;
	return len;
}

int MapSectionWriteOnMemory(char *stream, int curpos, char *output, int *bitWrite)
{
	int symbLen = (int)stream[curpos] + 1;
	int rPos = Get8BytePosition(curpos + symbLen - 1);
	int wPos = Get8BytePosition((*bitWrite) + symbLen - 1);
	int *sectionSize = (int *)(stream + rPos);

	BitWriteOnMemory(stream + curpos, output, bitWrite, symbLen);
	while (*bitWrite < wPos)
	{
		output[(*bitWrite) ++] = 0;
	}
	BitWriteOnMemory(stream + rPos, output, bitWrite, 8 + *sectionSize);
	return rPos + 8 + *sectionSize - curpos;
}

int NoxMapFlagChecking(char *stream, char *output, int *bitwr)
{
	int flag = *((int *)stream);
	int readCount = 0;

	if (flag ^ 2)
		return BitWriteOnMemory(stream, output, bitwr, 6);
	else
	{
		if (stream[4])
		{
			readCount += (5 + stream[4]);
			if (stream[readCount])
			{
				readCount += (1 + stream[readCount]);
			}
		}
		return BitWriteOnMemory(stream, output, bitwr, readCount);
	}
}

int NoxMapSectionSkip(char *stream, char *symb, int curpos)
{
	char *ptr = stream + curpos;
	int symbLen = strlen(symb) + 1, pos;
	int *sectionSize = NULL;

	if (symbLen == (int)ptr[0])
	{
		if (CheckSymbolMatching(ptr + 1, symb, symbLen))
		{
			pos = Get8BytePosition(ptr + symbLen - stream);
			sectionSize = (int *)(stream + pos);
			pos += (8 + *sectionSize);
			return pos - curpos;
		}
	}
	return -1;
}

int NoxPairTileCompress(char *stream, int start, int end, char *output)
{
	unsigned char temp[0x100];
	tPoint pHead;
	int cur = start, xyProfile, dur = 0;
	int bitWrite, destWrite = 0;
	unsigned char xProfile, yProfile, edgeCount;

	pHead.x = 0;
	pHead.pNext = NULL;
	while (cur < end)
	{
		bitWrite = 0;
		xyProfile = *((int *)(stream + cur));
		yProfile = stream[cur];
		xProfile = stream[cur + 1];
		edgeCount = stream[cur + 7];
		//cur += 8;
		cur += BitWriteOnMemory(stream + cur, temp, &bitWrite, 8);
		if (edgeCount)
		{
			//cur += (edgeCount * 5);
			cur += BitWriteOnMemory(stream + cur, temp, &bitWrite, edgeCount * 5);
		}
		if (xProfile & yProfile & 0x80)
		{
			AddTilePointList(&pHead, xyProfile, 0);
			edgeCount = stream[cur + 5];
			//cur += 6;
			cur += BitWriteOnMemory(stream + cur, temp, &bitWrite, 6);
			if (edgeCount)
			{
				//cur += (edgeCount * 5);
				cur += BitWriteOnMemory(stream + cur, temp, &bitWrite, edgeCount * 5);
			}
			BitWriteOnMemory(temp, output, &destWrite, bitWrite);
		}
		else
		{
			if (CheckDuplicateCoor(&pHead, xyProfile))
				dur++;
			else
			{
				BitWriteOnMemory(temp, output, &destWrite, bitWrite);
			}
		}
	}
	//memcpy_s(output, sizeof(int), &destWrite, sizeof(int)); //Write output file size
	RemoveAllPointList(pHead.pNext);
	return destWrite;
}

void TileSectionEndSig(char *output, int *bitwr)
{
	char endSig[] = { 0xff, 0xff, 0x00, 0x00 };
	
	BitWriteOnMemory(endSig, output, bitwr, 2);
}

int NoxMapFloorSectionCompress(char *stream, char *symb, int curpos, char *output, int *bitwr)
{
	char *ptr = stream + curpos;
	int symbLen = strlen(symb) + 1, pos;
	int *sectionSize = NULL, resSize;

	if (symbLen == (int)ptr[0])
	{
		if (CheckSymbolMatching(ptr + 1, symb, symbLen))
		{
			pos = Get8BytePosition(ptr + symbLen - stream);
			sectionSize = (int *)(stream + pos);
			BitWriteOnMemory(ptr, output, bitwr, pos - curpos + 0x1a);
			resSize = NoxPairTileCompress(stream, pos + 0x1a, *sectionSize + pos + 6, output + *bitwr) + 0x12;
			*bitwr += (resSize - 0x12);
			TileSectionEndSig(output, bitwr);
			resSize += 2;
			memcpy_s(output + pos, sizeof(int), &resSize, sizeof(int));
			pos += (8 + *sectionSize);
			return pos - curpos;
		}
	}
	return -1;
}

void WritePeddingBytes(char *output, int *bitwr)
{
	int checkSum = 8 - (*bitwr % 8);

	for (int i = 0; i < checkSum; i++)
		output[(*bitwr)++] = 0;
}

int NoxMapCompressMain(char *stream, char *output)
{
	int bitwr = 0, curpos = 0;

	curpos += BitWriteOnMemory(stream + curpos, output, &bitwr, 0x5a2);
	curpos += NoxMapFlagChecking(stream + curpos, output, &bitwr);

	for (int i = 0; i < 15; i++)
	{
		switch (i)
		{
		case 1:
			curpos += NoxMapFloorSectionCompress(stream, PickMyStringFromIndex(161), curpos, output, &bitwr);
			break;
		case 10:
			curpos += PolygonSectionCompress(stream, PickMyStringFromIndex(147), curpos, output, &bitwr);
			break;
		default:
			curpos += MapSectionWriteOnMemory(stream, curpos, output, &bitwr);
		}
	}
	WritePeddingBytes(output, &bitwr);
	return bitwr;
}

int WriteCompressedMapFileNotCrypted(char *outfName, char *stream)
{
	//char *decrypt = NULL;
	int streamLen = *((int *)stream);
	FILE *fp = NULL;

	fopen_s(&fp, outfName, "wb");
	if (fp != NULL)
	{
		if (streamLen)
		{
			//decrypt = NoxAPICrypt(stream + 4, NOX_MAP, streamLen, 1);
			fwrite(stream + 4, (size_t)streamLen, sizeof(char), fp);
			//free(decrypt);
		}
		fclose(fp);
		return 1;
	}
	return 0;
}

int WriteCompressedMapFile(char *outfName, char *stream)
{
	char *decrypt = NULL;
	int streamLen = *((int *)stream);
	FILE *fp = NULL;

	fopen_s(&fp, outfName, "wb");
	if (fp != NULL)
	{
		if (streamLen)
		{
			decrypt = NoxAPICrypt(stream + 4, NOX_MAP, streamLen, 1);
			fwrite(decrypt, (size_t)streamLen, sizeof(char), fp);
			free(decrypt);
		}
		fclose(fp);
		return 1;
	}
	return 0;
}

int NoxMapCompressInit(char *fName, char *outfName)
{
	int fSize, dSize;
	char *stream = NULL, *output = NULL;

	fSize = ReadNoxMapFileStream(fName, &stream);
	if (fSize + 1)
	{
		output = (char *)malloc(sizeof(char) * fSize + 4);
		memset(output, 0, sizeof(int));
		dSize = NoxMapCompressMain(stream, output + 4);
		if (dSize)
		{
			memcpy_s(output, sizeof(int), &dSize, sizeof(int));
			if (fSize ^ dSize)
			{
				WriteCompressedMapFile(outfName, output);
				return 1;
			}
			//WriteCompressedMapFileNotCrypted(outfName, output);
			//return 1;
		}
		free(stream);
		free(output);
	}
	return 0;
}