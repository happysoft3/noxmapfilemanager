
#include "filterEditDlg.h"
#include "ctrlDraw.h"


BOOL CALLBACK FilterEditDlgProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch (iMessage)
	{
	case WM_DRAWITEM:
		//DrawMyDialogStaticItems((LPDRAWITEMSTRUCT)lParam);
		OnDrawItem(hDlg, (DRAWITEMSTRUCT *)lParam);
		break;
	case WM_INITDIALOG:
		FilterEditDlgInit(MainProcPtr, hDlg);
		return 1;
		/*case WM_CTLCOLORLISTBOX:
		return GetMyBrush(MainProcPtr, (HDC)wParam);*/
	case WM_COMMAND:
		switch (wParam)
		{
		case FILTER_EDIT_BUTN1:
			FilterEditComplete(MainProcPtr, hDlg);
		case FILTER_EDIT_BUTN2:
			EndDialog(hDlg, 0);
			break;
		case FILTER_EDIT_BUTN3:
			FilterEditSettingReset(hDlg);
		}
	}
	return 0;
}

void FilterEditDlgInit(MyMainProc *mProc, HWND hDlg)
{
	int flag = mProc->typeFilterFlag, dlgCheck = IDC_CHECK1, pic = 1;

	for (int i = 0; i < 8; i++)
	{
		SendDlgItemMessage(hDlg, dlgCheck + i, BM_SETCHECK, flag & pic, 0);
		SetWindowText(GetDlgItem(hDlg, dlgCheck + i), PickMyWideStringFromIndex(54 + i));
		pic <<= 1;
	}
	SetWindowText(GetDlgItem(hDlg, FILTER_EDIT_BUTN1), PickMyWideStringFromIndex(50));
	SetWindowText(GetDlgItem(hDlg, FILTER_EDIT_BUTN2), PickMyWideStringFromIndex(51));
	SetWindowText(GetDlgItem(hDlg, FILTER_EDIT_BUTN3), PickMyWideStringFromIndex(64));
	SetWindowText(hDlg, PickMyWideStringFromIndex(52));
	SetWindowText(GetDlgItem(hDlg, IDC_STATIC0), PickMyWideStringFromIndex(53));
}

void FilterEditComplete(MyMainProc *mProc, HWND hDlg)
{
	int pic = 1, dlgCheck = IDC_CHECK1;
	int flagUpdate = 0;

	for (int i = 0; i < 8; i++)
	{
		flagUpdate |= ((int)SendDlgItemMessage(hDlg, dlgCheck + i, BM_GETCHECK, 0, 0) * pic);
		pic <<= 1;
	}
	if (flagUpdate == 0xff)
		flagUpdate |= 0x100;
	mProc->typeFilterFlag = flagUpdate;
}

void FilterEditSettingReset(HWND hDlg)
{
	int dlgCheck = IDC_CHECK1;

	for (int i = 0; i < 8; i ++)
		SendDlgItemMessage(hDlg, dlgCheck + i, BM_SETCHECK, 1, 0);
	MyQuestionBox(hDlg, 62, 63, MB_OK | MB_ICONINFORMATION);
}