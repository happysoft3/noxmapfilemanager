#pragma once

#include "mainHead.h"

int ReadNoxMapFileStream(char *mapFileName, char **destPtr);
int CheckSymbolMatching(char *stream, char *symb, int symbLen);
int Get8BytePosition(int curPos);
int GetQuestIntro(char *stream, int *sCur, char **questIntro);
int GetMapWallsCount(char *stream, int *sCur);
int NoxTileSearching(char *stream, int start, int end, int *getEdge);
int GetMapTilesCount(char *stream, int *sCur, int *getEdge);
int GetMapSecretWallsCount(char *stream, int *sCur);
int GetMapBreakWallCount(char *stream, int *sCur);
int GetMapWaypoints(char *stream, int *sCur);
int GetMapDebugData(char *stream, int *sCur);
int GetMapWindowWalls(char *stream, int *sCur);
int GetMapGroupCount(char *stream, int *sCur);
int MapScriptInfo(char *stream, int *sCur, int *strCount);
int MapAmbientData(char *stream, int *sCur);
int GetPolygonCount(char *stream, int *sCur, int *getPointCount);
int MapIntroField(char *stream, int *sCur);
int ScriptDataField(char *stream, int *sCur);
int ReadMapSectionObjectTOC(char *stream, int *sCur);
int ReadMapSectionObjectData(char *stream, int *sCur, int fSize);