#pragma once

#include "mainHead.h"

BOOL CALLBACK FilterEditDlgProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam);

void FilterEditDlgInit(MyMainProc *mProc, HWND hDlg);
void FilterEditComplete(MyMainProc *mProc, HWND hDlg);
void FilterEditSettingReset(HWND hDlg);