#pragma once

#include "mainHead.h"

typedef enum NoxCryptFormat
{
	NOX_SAVE,
	NOX_MAP,
	NOX_THING,
	NOX_MODIFIER,
	NOX_GAMEDATA,
	NOX_MONSTER,
	NOX_SOUNDSET,
	NONE
}NoxFormat;

void *NoxAPICrypt(unsigned char *data, NoxFormat format, int dataLength, int mode);