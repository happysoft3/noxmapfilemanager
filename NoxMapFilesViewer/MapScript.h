#pragma once

#include "mainHead.h"

int ReadStringSection(char *stream, int *sCur);
int ReadFunctionSection(char *stream, int *sCur);
int MapScriptStreamEntry(char *stream, int *strCount);