
#include "MapScript.h"
#include "readMapDetail.h"

int ReadStringSection(char *stream, int *sCur)
{
	char *ptr = stream + *sCur;
	char *v3Script = PickMyStringFromIndex(144);
	int *strCount, v3ScrLen = PickMyStringLenFromIndex(144);
	int v3ScrFlag = 0;

	if (CheckSymbolMatching(ptr, PickMyStringFromIndex(140), PickMyStringLenFromIndex(140)))
	{
		ptr += PickMyStringLenFromIndex(140);
		strCount = (int *)ptr;
		ptr += 4;
		if (strCount)
		{
			if (*((int*)ptr) >= v3ScrLen)
				v3ScrFlag = 0x10000 * (CheckSymbolMatching(ptr + 4, v3Script, v3ScrLen));
		}
		for (int i = 0; i < *strCount; i++)
			ptr += (4 + *((int *)ptr));
		*sCur = ptr - stream;
		return *strCount | v3ScrFlag;
	}
	return -1;
}

int ReadFunctionSection(char *stream, int *sCur)
{
	char *ptr = stream + *sCur;
	int *funcCount = (int *)ptr;

	if (CheckSymbolMatching(ptr, PickMyStringFromIndex(141), PickMyStringLenFromIndex(141)))
	{
		ptr += PickMyStringLenFromIndex(141);
		*sCur = ptr - stream;
		return *((int *)ptr);
	}
	return -1;
}

int MapScriptStreamEntry(char *stream, int *strCount)
{
	int sCur = 0, funcCount = -1;

	if (CheckSymbolMatching(stream, PickMyStringFromIndex(139), PickMyStringLenFromIndex(139)))
	{
		sCur += PickMyStringLenFromIndex(139);
		*strCount = ReadStringSection(stream, &sCur);
		funcCount = ReadFunctionSection(stream, &sCur);
	}
	else
		*strCount = 0;
	return funcCount;
}