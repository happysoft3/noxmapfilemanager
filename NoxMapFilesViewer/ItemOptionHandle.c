
#include "ItemOptionHandle.h"
#include "myNodeKey.h"



void DisplayFileInfo(HWND hListView, HWND hEdit, int row, int col)
{
	wchar_t mapInfo[100] = { 0, };

	ListView_GetItemText(hListView, row, col, mapInfo, sizeof(mapInfo));
	SetWindowText(hEdit, mapInfo);
}

void WriteMyEditWindow(HWND hEdit, char *content)
{
	wchar_t wideStr[100] = { 0, };

	MyWideCharConvert(content, (char *)wideStr, strlen(content) + 1);
	SetWindowText(hEdit, wideStr);
}

int ReadMapFileForGetInfo(char *mapFileName, NoxMapNode *mapNode)
{
	FILE *mapFp = NULL;

	fopen_s(&mapFp, mapFileName, PickMyStringFromIndex(111));
	if (mapFp != NULL)
	{
		GetMapTitleInfo(mapFp, mapNode->mapTitle);			//Ÿ��Ʋ ���� ������
		GetMapDetailInfo(mapFp, mapNode->mapAuthor, 0x40, 0x282);	//������ ���� ������
		GetMapDetailInfo(mapFp, mapNode->mapVersion, 0x10, 0x272);	//�� �������� ������
		GetMapDetailInfo(mapFp, mapNode->mapDate, 0x20, 0x582);		//�� ���۳�¥ ����
		fclose(mapFp);
		return 1;
	}
	return 0;
}

void DisplayAllEmpty(MainFormWnd *mWnd)
{
	if (GetWindowTextLength(mWnd->fileNameEdit))
	{
		SetWindowText(mWnd->fileNameEdit, L"");
		SetWindowText(mWnd->mapTypeEdit, L"");
		SetWindowText(mWnd->mapTitleEdit, L"");
		SetWindowText(mWnd->mapMakerEdit, L"");
		SetWindowText(mWnd->mapVerEdit, L"");
		SetWindowText(mWnd->mapDateEdit, L"");
	}
}

void DisplaySelectColumnData(MyMainProc *mProc, int iItem)
{
	char mapDirectory[0x100] = { 0, };
	MainFormWnd *mWnd = mProc->mFormPtr;
	HWND hList = mWnd->mapList;
	NoxMapNode *mapNodePtr = NULL;

	mProc->prevSelectIndex = iItem;
	mProc->mapViewSelectIndex = iItem;
	if (iItem + 1)
	{
		DisplayFileInfo(hList, mProc->mFormPtr->fileNameEdit, iItem, 0);
		DisplayFileInfo(hList, mProc->mFormPtr->mapTypeEdit, iItem, 1);

		mapNodePtr = (NoxMapNode *)GetMapNodePtr(mProc->mFormPtr->mapList, iItem, mProc->Lkey, mProc->Rkey);
		if (mapNodePtr != NULL)
		{
			sprintf_s(mapDirectory, sizeof(mapDirectory), PickMyStringFromIndex(84), mProc->noxDirectory, mapNodePtr->mapFileName, mapNodePtr->mapFileName);
			if (ReadMapFileForGetInfo(mapDirectory, mapNodePtr))
			{
				WriteMyEditWindow(mWnd->mapTitleEdit, mapNodePtr->mapTitle);
				WriteMyEditWindow(mWnd->mapMakerEdit, mapNodePtr->mapAuthor);
				WriteMyEditWindow(mWnd->mapVerEdit, mapNodePtr->mapVersion);
				WriteMyEditWindow(mWnd->mapDateEdit, mapNodePtr->mapDate);
			}
		}
	}
	else
		DisplayAllEmpty(mWnd);
}
