#pragma once

#include "mainHead.h"

//TODO: 메인 Form UI 구현입니다

void MyListViewMakeColumn(HWND hLstView);
void MainFormOnCreateHandler(MyMainProc *mProc, HWND hWnd);

void DrawMapCounting(HWND hWnd);
void DisplayMapsCount(MyMainProc *mProc, HDC *hdc);

void MainFormOnDrawHandler(MyMainProc *mProc, HWND hWnd);
void AddMapListViewItem(HWND hMapList, int row, int col, char *itemName);
void AddMapNodeAddressItem(HWND hMapList, int row, int col, char *itemName);
void UpdateMapsListView(MyMainProc *mProc, HWND hWnd);
void NextMapFocusOnList(HWND hList, int *cursel, int ItemCounts);
void ExcludeSelectedMap(MyMainProc *mProc, HWND hWnd);

void ShowDlgFilteredMapsList(MyMainProc *mProc, HWND hWnd);
void ShowFilterEditorDialog(MyMainProc *mProc, HWND hWnd);

void MainFormOnClose(MyMainProc *mProc);