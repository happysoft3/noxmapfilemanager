#pragma once

#include "mainHead.h"

void ClearAllMapNodes(NoxMapNode *removedNode);
void EmptyMapNodes(MyMainProc *mProc);

int ExcludeThisMap(HWND hList, int sel, int key1, int key2);

void OpenDetailViewDlg(MyMainProc *mProc, HWND hWnd);

void MapfileViewerInit(MyMainProc *mProc);
void MapfileViewerExit(MyMainProc *mProc);


void ClearNodeArrays(MyMainProc *mProc);
void BuildNodeArray(MyMainProc *mProc);
void RebuildNodeArrayWhenDecrease(MyMainProc *mProc, int idx, NoxMapNode *removeNode);