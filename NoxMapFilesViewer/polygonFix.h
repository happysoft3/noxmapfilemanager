#pragma once

#include "mainHead.h"

int PolygonHandler(char *stream, char *output, int *bitwr, int questMode);
int PolygonSectionCompress(char *stream, char *symb, int curpos, char *output, int *bitwr);