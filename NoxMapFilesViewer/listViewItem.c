
#include "listViewItem.h"
#include "NoxCryptApi.h"
#include "myNodeKey.h"

void GetMapFolderName(char *fullDir, char *dest, int destSize)
{
	int srcLen = (int)strlen(fullDir);
	for (int i = 0; i < srcLen; i++)
	{
		if (fullDir[i] == '\\')
		{
			GetMapFolderName(fullDir + i + 1, dest, destSize);
			return;
		}
	}
	strcpy_s(dest, (size_t)destSize, fullDir);
}

int GetNoxMapFileSize(FILE *fp)
{
	int size;

	fseek(fp, 0, 2);
	size = ftell(fp);
	fseek(fp, 0, 0);
	return size;
}

int GetNoxMapType(FILE *fp)
{
	char *res = NULL, typeFlag[8] = { 0, };
	int *retFlag = NULL;

	fseek(fp, 0x5a0, 0);
	fread_s(typeFlag, sizeof(typeFlag), sizeof(char), sizeof(typeFlag), fp);
	res = (char *)NoxAPICrypt(typeFlag, NOX_MAP, 8, 0);

	retFlag = (int *)(res + 2);
	free(res);

	return *retFlag;
}

void GetMapTitleInfo(FILE *fp, char *title)
{
	char titleReadRaw[0x48] = { 0, }, *titleRead = NULL;

	fseek(fp, 0x30, 0);
	fread_s(titleReadRaw, sizeof(titleReadRaw), sizeof(char), sizeof(titleReadRaw), fp);
	titleRead = (char *)NoxAPICrypt(titleReadRaw, NOX_MAP, (int)sizeof(titleReadRaw), 0);

	memcpy_s(title, 0x40, titleRead + 2, 0x40);
	title[0x40] = 0;
	free(titleRead);
}

void GetMapDetailInfo(FILE *fp, char *destInfo, int destLen, int offset)
{
	int align = offset % 4;
	char readFieldRawData[0x48] = { 0, }, *decryptStream = NULL;

	fseek(fp, offset - align, 0);
	fread_s(readFieldRawData, sizeof(readFieldRawData), sizeof(char), sizeof(readFieldRawData), fp);
	decryptStream = (char *)NoxAPICrypt(readFieldRawData, NOX_MAP, (int)sizeof(readFieldRawData), 0);

	memcpy_s(destInfo, (size_t)destLen, decryptStream + align, (size_t)destLen);
	destInfo[destLen] = 0;
	free(decryptStream);
}

int InsertMapTypeName(char *destStr, int typeFlag, int destStrLen)
{
	int typeCheckFlag = 0;
	char *typeSelect = NULL;

	switch (typeFlag)
	{
	case 0x01:
		typeSelect = PickMyStringFromIndex(16);	//solo
		typeCheckFlag = 1;
		break;
	case 0x02:
		typeSelect = PickMyStringFromIndex(17);	//quest
		typeCheckFlag = 2;
		break;
	case 0x03:
		typeSelect = PickMyStringFromIndex(18);	//solo_t
		typeCheckFlag = 4;
		break;
	case 0x08:
		typeSelect = PickMyStringFromIndex(21);	//ctf_8
		typeCheckFlag = 8;
		break;
	case 0x18:
		typeSelect = PickMyStringFromIndex(20);	//ctf
		typeCheckFlag = 0x10;
		break;
	case 0x34:
		typeSelect = PickMyStringFromIndex(19); //arena
		typeCheckFlag = 0x20;
		break;
	case 0x40:
		typeSelect = PickMyStringFromIndex(23); //flagball
		typeCheckFlag = 0x40;
		break;
	default:
		if (typeFlag == 0x80000000)
		{
			typeSelect = PickMyStringFromIndex(22); //social
			typeCheckFlag = 0x80;
		}
		else
		{
			typeSelect = PickMyStringFromIndex(24);	//any
			typeCheckFlag = 0x100;
		}
	}
	strcpy_s(destStr, (size_t)destStrLen, typeSelect);
	return typeCheckFlag;
}

int GetMapFileInfo(NoxMapNode *currentMap, char *fullDirName, int filterFlag)
{
	int nodeSave = 0;
	FILE *mapFp = NULL;

	fopen_s(&mapFp, fullDirName, PickMyStringFromIndex(88));
	if (mapFp != NULL)
	{
		currentMap->typeFlag = GetNoxMapType(mapFp);			//지도타입 저장
		if (InsertMapTypeName(currentMap->mapTypeName, currentMap->typeFlag, sizeof(currentMap->mapTypeName)) & filterFlag)
		{
			currentMap->mapFileSize = GetNoxMapFileSize(mapFp);		//지도파일 크기저장
			nodeSave = 1;
		}
		fclose(mapFp);
	}
	return nodeSave;
}

int MapFileScan(NoxMapNode **pic, int **argList, int filterFlag)
{
	int lowKey = *argList[0], highKey = *argList[1];
	NoxMapNode *updatedMap = (NoxMapNode *)malloc(sizeof(NoxMapNode));
	char *mapName = (char *)argList[2];
	char *fullDirName = (char *)argList[3];
	char mapFullDir[200] = { 0, };

	memset(updatedMap->nodeStream, 0, sizeof(updatedMap->nodeStream));				//Add
	strcpy_s(updatedMap->mapFileName, sizeof(updatedMap->mapFileName), mapName);	//TODO: 맵 노드에 지도명 정보 저장
	sprintf_s(mapFullDir, sizeof(mapFullDir), PickMyStringFromIndex(87), fullDirName, mapName);	//TODO: 지도 파일이 폴더에 없는 경우 이 노드는 삭제됨
	if (GetMapFileInfo(updatedMap, mapFullDir, filterFlag))
	{
		if (*pic != NULL)						//TODO: 리스트 연결작업
		{
			updatedMap->nextMap = *pic;
			(*pic)->prevMap = updatedMap;
			updatedMap->nodeIndex = (*pic)->nodeIndex + 1;
		}
		else
		{
			updatedMap->nextMap = NULL;
			updatedMap->nodeIndex = 0;
		}
		updatedMap->prevMap = NULL;
		strcpy_s(updatedMap->mapFolderDir, sizeof(mapFullDir), fullDirName);
		updatedMap->randomKey = GenerateRandomValue(0x10000007, 0xfffffff7);
		updatedMap->Lkey = lowKey;
		updatedMap->Rkey = highKey;
		NodePtrEncrypt(updatedMap, updatedMap->nodeStream, updatedMap->Lkey, updatedMap->Rkey);	//Add
		updatedMap->Lkey = 0;		//Add
		updatedMap->Rkey = 0;		//Add
		*pic = updatedMap;
		return 1;
	}
	else
		free(updatedMap);
	return 0;
}

void ScanTargetDirectory(MyMainProc *mProc, char *targDir)
{
	WIN32_FIND_DATAA fileData;
	HANDLE handle;
	char fullDir[200] = { 0, };
	int fullDirLen;
	int **argList = (int **)malloc(sizeof(int) * 4);

	handle = FindFirstFileA(targDir, &fileData);
	if (handle == INVALID_HANDLE_VALUE)
	{
		return; //TODO: ERROR
	}
	sprintf_s(fullDir, sizeof(fullDir), PickMyStringFromIndex(86), mProc->noxDirectory);
	fullDirLen = strlen(fullDir);
	EmptyMapNodes(mProc);
	argList[0] = &mProc->Lkey;
	argList[1] = &mProc->Rkey;
	argList[2] = (int *)&fileData.cFileName;
	argList[3] = (int *)&fullDir;
	do
	{
		if (CheckSkipScannigFile(&fileData))
		{
			memcpy_s(fullDir + fullDirLen, strlen(fileData.cFileName) + 1, fileData.cFileName, strlen(fileData.cFileName) + 1);
			//mProc->mapCount += MapFileScan(&mProc->firstMapPtr, fullDir, mProc->Lkey, mProc->Rkey, mProc->typeFilterFlag);
			mProc->mapCount += MapFileScan(&mProc->firstMapPtr, argList, mProc->typeFilterFlag);
		}
	} while (FindNextFileA(handle, &fileData));
	FindClose(handle);
	free(argList);
}

void MakeMapNodeList(MyMainProc *mProc)
{
	char mapDir[200] = { 0, };

	mProc->Lkey = GenerateRandomValue(0x10000077, 0xffffff77);
	mProc->Rkey = GenerateRandomValue(0x10000077, 0xffffff77);
	sprintf_s(mapDir, sizeof(mapDir), PickMyStringFromIndex(79), mProc->noxDirectory);
	ScanTargetDirectory(mProc, mapDir);
}

void RemoveCurrentNode(NoxMapNode **firstNode, NoxMapNode *currentNode)
{
	if (currentNode->nextMap != NULL)
		currentNode->nextMap->prevMap = currentNode->prevMap;
	else if (currentNode == *firstNode)
		*firstNode = currentNode->prevMap;
	if (currentNode->prevMap != NULL)
		currentNode->prevMap->nextMap = currentNode->nextMap;
	else if (currentNode == *firstNode)
		*firstNode = currentNode->nextMap;
	free(currentNode);
}