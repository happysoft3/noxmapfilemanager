
#include "NoxCryptApi.h"
#include "myNodeKey.h"
#include <time.h>

int GetMapNodePtr(HWND hListView, int row, int Lkey, int Hkey)
{
	char keyData[8] = { 0, };
	int *keyPic = (int *)keyData; //, lowData = 0, highData = 0;
								  //wchar_t readRawData[28];
	char cryptedData[20] = { 0, };

	keyPic[0] = Lkey;
	keyPic[1] = Hkey;

	//ListView_GetItemText(hListView, row, 3, readRawData, sizeof(readRawData));
	ListView_GetItemText(hListView, row, 3, (LPWSTR)cryptedData, sizeof(cryptedData));
	//swscanf_s(readRawData, L"%x %x", &lowData, &highData);

	//WideCharToMultiByte(CP_ACP, 0, readRawData, sizeof(readRawData), cryptedData, sizeof(cryptedData), NULL, NULL);
	//keyPic = (int *)cryptedData;
	//keyPic[0] = lowData;
	//keyPic[1] = highData;

	return GetNodeRealPtr(cryptedData, keyData);
}


void KeyRandomInit()
{
	srand(time(0));
}

int GenerateRandomValue(int min, int max)
{
	return (rand() % (max - min)) + min;
}

void StreamEncrypt(char *stream, int lowKey, int highKey)
{
	int *low = (int *)stream, *high = (int *)(stream + 4);

	*low = (~(*low)) ^ lowKey;
	*high = (~(*high)) ^ highKey;
}

void StreamDecrypt(char *stream, int lowKey, int highKey)
{
	int *low = (int *)stream, *high = (int *)(stream + 4);

	*low = ~((*low) ^ lowKey);
	*high = ~(*high ^ highKey);
}

void NodePtrEncrypt(NoxMapNode *currentNode, char *cryptDestData, int lowKey, int highKey) //unless function. not yet...
{
	unsigned int rawStream[2] = { 0, }, Lidx = 0, Ridx = 4;
	char stream[8] = { 0, }, *picker = (char *)&rawStream;
	char *resultStream = NULL;

	rawStream[0] = (int)currentNode;
	rawStream[1] = currentNode->randomKey;
	for (int i = 0; i < 8; i++)
	{
		//Left: E E E E, o o o o
		if (i % 2) //odd
		{
			stream[Ridx++] = picker[i];
		}
		else //even
		{
			stream[Lidx++] = picker[i];
		}
	}
	StreamEncrypt(stream, lowKey, highKey);
	resultStream = NoxAPICrypt(stream, NOX_SOUNDSET, 8, 1);
	memcpy_s(cryptDestData, (size_t)8, resultStream, (size_t)8);
	free(resultStream);
}

int GetNodeRealPtr(char *cryptData, char *keyData)
{
	int *temp = NULL, *key = (int *)keyData, res = 0;
	char *stream = NodePtrDecrypt(cryptData, key[0], key[1]);

	temp = (int *)stream;
	res = *temp;
	free(stream);

	return res;
}

int GetNodeRealPtrOther(char *cryptData, int Lkey, int Hkey)
{
	int *temp = NULL, res;
	char *stream = NodePtrDecrypt(cryptData, Lkey, Hkey);

	temp = (int *)stream;
	res = *temp;
	free(stream);

	return res;
}

char* NodePtrDecrypt(char *cryptData, int lowKey, int highKey)
{
	char *output = (char *)malloc(sizeof(char) * 8);
	char *stream = NoxAPICrypt(cryptData, NOX_SOUNDSET, 8, 0);
	char *Lpic = stream, *Rpic = stream + 4;

	memset(output, 0, (size_t)8);
	StreamDecrypt(stream, lowKey, highKey);
	for (int i = 0; i < 8; i++)
	{
		if (i % 2)
		{
			output[i] = *Rpic++;
		}
		else
		{
			output[i] = *Lpic++;
		}
	}
	free(stream);
	return output;
}