//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// NoxMapFilesViewer.rc에서 사용되고 있습니다.
//
#define IDI_ICON1                       101
#define IDD_DIALOG1                     102
#define IDD_DIALOG2                     104
#define IDD_DIALOG3                     106
#define IDC_LIST1                       1001
#define IDC_BUTTON1                     1002
#define IDC_BUTTON2                     1003
#define IDC_CHECK1                      1003
#define IDC_CHECK2                      1004
#define IDC_CHECK3                      1005
#define IDC_CHECK4                      1006
#define IDC_CHECK5                      1007
#define IDC_CHECK6                      1008
#define IDC_CHECK7                      1009
#define IDC_CHECK8                      1010
#define IDC_STATIC0                     1011
#define FILTER_EDIT_BUTN1               1012
#define FILTER_EDIT_BUTN2               1013
#define FILTER_EDIT_BUTN3               1014
#define EXMAP_COUNT_TXT                 1014
#define ANALYSIS_EDIT                   1015
#define ANALYSIS__BUTTON1               1016
#define ANALYSIS__BUTTON2               1017
#define ANALYSIS__BUTTON3               1018
#define ANALYSIS__BUTTON4               1019
#define ANALYSIS_STATIC                 1020
#define ANALYSIS__BUTTON5               1021

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        108
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1021
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
